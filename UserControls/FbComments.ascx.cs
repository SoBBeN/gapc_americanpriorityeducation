﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FbComments : System.Web.UI.UserControl
{
    protected string dataHref = "http://example.com";
    protected string dataWidth = "650";
    protected string dataNumPosts = "10";

    public string Href
    {
        get { return dataHref; }
        set { dataHref = value; }
    }
    public string Width
    {
        get { return dataWidth; }
        set { dataWidth = value; }
    }
    public string NumPosts
    {
        get { return dataNumPosts; }
        set { dataNumPosts = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }



}