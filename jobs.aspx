﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="jobs.aspx.cs" Inherits="jobs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        #main {
            background-color: #1d2f4c;
            border: 1px solid #1d2f4c;
            padding: 5px 15px 15px 15px;
        }
        #job_wrapper {
            color: #fff;
            width:100%;
            padding-top:15px;
        }
         #job_wrapper .job_title {
            width:48%;
            float:left;
        }
         #job_wrapper .job_title label{
            font-size: 19px;
            font-weight: bold;
                margin-bottom: 5px;
        }
        #job_wrapper .job_title input{
                width: 95%;
                margin-top: 5px;
                padding: 5px 0px 5px 10px;
                border-radius: 5px;
        }

        #job_wrapper .job_location_input {
            width:48%;
            float:right;
        }

        #job_wrapper .job_location_input label{
            font-size: 19px;
            font-weight: bold;
            margin-bottom: 5px;
        }

        #job_wrapper .job_location_input input{
            width: 95%;
            margin-top: 5px;
            padding: 5px 0px 5px 10px;
            border-radius: 5px;
        }
        #job_wrapper  button{
            border-radius: 5px;
            border-color: #00BA00;
            background-color: #00BA00;
            padding: 5px 5px;
            width: 140px;
            font-size: 16px;
            font-weight: bold;
            color: white;
            margin-top: 10px;
            float: right;
            text-shadow: 1px 1px 10px black;
            cursor: pointer;
        }
        #job_wrapper  h1{
            position: unset;
            color: white;
            line-height:unset;
            margin-top:unset;
            margin-bottom:5px;
        }
        #job_wrapper  .jobs_list_wrapper{
            background-color: white;
            
        }
        #job_wrapper .jobs_list_wrapper .job  {
            margin-top:10px;
            padding:10px;
            
        }
        #job_wrapper .jobs_list_wrapper .job  a{
            color:#1a0dab;
            font-size:15px;
            font-weight:bold;
            
        }
        #job_wrapper  .jobs_list_wrapper .job .job_location {
            color: #83b14e;
            font-size: 12px;
            padding: 8px 0 5px;
        }
        #job_wrapper .jobs_list_wrapper .job .job_desc {
            color: #545459;
            font-size: 14px;
            font-weight: bold;
        }

        #job_wrapper .pager-count {
            text-align: center;
            font-size: 9pt;
            font-weight: normal;
            color: #666;
            top: 4px;
            margin-left: 30px;
        }
        #job_wrapper .paging {
            text-align: center;
            margin-top: 20px;
        }
        #job_wrapper .paging_left, .paging_right {
            display: inline-block;
        }
        #job_wrapper ul.ul-pager {
            clear: both;
            float: left;
            margin: 0;
            padding: 0;
        }
        #job_wrapper ul.ul-pager li {
            display: block;
            float: left;
            margin: 0 2px;
            padding: 4px 0;
            border-style: solid;
            border-width: 1px;
            border-color: #212154;
            width: 22px;
            text-align: center;
            background-color: #1d2f4c;
            font-size: 9pt;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            -moz-box-shadow: 2px 2px 6px rgba(0,0,0,.6);
            -webkit-box-shadow: 2px 2px 6px rgba(0,0,0,.6);
            box-shadow: 2px 2px 6px rgba(0,0,0,.6);
        }
        #job_wrapper ul.ul-pager li a {
            color:white;
        }
        #job_wrapper ul.ul-pager li.selected {
            background-color: #fff;
            color: #212154;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            -moz-box-shadow: 2px 2px 6px rgba(0,0,0,.6);
            -webkit-box-shadow: 2px 2px 6px rgba(0,0,0,.6);
            box-shadow: 2px 2px 6px rgba(0,0,0,.6);
        }
        #job_wrapper a.pager-a {
            color: #36679e;
            font-weight: bold;
            position: relative;
            top: -10px;
            text-decoration: underline;
       }
        @media(max-width:750px){
            #job_wrapper .job_location_input,#job_wrapper .job_title {
                width:100%;
                float:unset;
            }
            #job_wrapper .job_title input {
                margin-bottom: 10px;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">

    <div id="job_wrapper">
        <div class="job_title">
            <label>Job Title or Keyword</label>
            <br />
            <input runat="server" id="title" class="" type="text" name="q" placeholder="Job Title or Keyword" />

        </div>
        <div class="job_location_input">
            <label>City, State or Zip</label>
            <br />

             <input runat="server" id="location" class="" type="text" name="l" placeholder="City, State or ZIP" />
        </div>
         <button type="submit" class="search_btn">Search Jobs</button>
        <div class="clear"></div>
      <h1 class="title"><asp:Literal runat="server" ID="litTitle"></asp:Literal></h1>
        <div class="jobs_list_wrapper">
            <%-- loop would be here start --%>
                    <asp:Literal runat="server" ID="litJobs"></asp:Literal>
                                    <br />
                        <asp:Literal runat="server" ID="litPaging"></asp:Literal>
            <%-- loop would be here end --%>
        </div>
    </div>
      
    
    </asp:Content>