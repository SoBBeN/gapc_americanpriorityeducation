﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;

namespace NSS.ROS
{
    public enum OfferType
    {
        External = 1,
        Internal = 2,
        Linkout = 3,
        Optin = 4,
        Static = 5,
        InternalLinkout = 6
    }

    public enum ImpressionProvider
    {
        Unspecified = 255,
        LandingPage = 0,
        StaticFlow = 1,
        DecisionEngine = 2,
        Randomizer = 3,
        ConfirmationPage = 4
    }

    public enum OfferUsage
    {
        LandingPage = 1,
        Offer = 2,
        ConfirmationPage = 3
    }


    /// <summary>
    /// Database interface.
    /// </summary>
    public static class ros
    {


        public static int InsertEXSSuccess(int SessionID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC InsertEXSSuccess ");
            sb.Append(sql.q(SessionID));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void InsertUnsub(string UnsubEmail, string UnsubPhone)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC InsertUnsub '");
            sb.Append(sql.q(UnsubEmail)).Append("','");
            sb.Append(sql.q(UnsubPhone)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_UpdateConversion(string OfferID, string SessionID, string Revenue)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateConversion '");
            sb.Append(sql.q(OfferID)).Append("','");
            sb.Append(sql.q(SessionID)).Append("','");
            sb.Append(sql.q(Revenue)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertCSVUpload(int OfferID, int LoginID, string FileName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertCSVUpload ");
            sb.Append(sql.q(OfferID)).Append(",");
            sb.Append(sql.q(LoginID)).Append(",'");
            sb.Append(sql.q(FileName)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static void SaveSurveyAnswer(int intSessionID, int intSurveyQuestionID, int intSurveyAnswerID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC SaveSurveyAnswer ");
            sb.Append(sql.q(intSessionID)).Append(",");
            sb.Append(sql.q(intSurveyQuestionID)).Append(",");
            sb.Append(sql.q(intSurveyAnswerID));
            sql.ExecNonQuery(sb.ToString());
        }

        public static void SaveAchieveCardResponse(int intSessionID, string Status, string Message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC SaveAchieveCardResponse ");
            sb.Append(sql.q(intSessionID)).Append(",'");
            sb.Append(sql.q(Status)).Append("','");
            sb.Append(sql.q(Message)).Append("'");
            sql.ExecNonQuery(sb.ToString());
        }

        public static void SaveNumberDrawResponse(int intSessionID, string DrawNum, string DrawConf)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC SaveNumberDrawResponse ");
            sb.Append(sql.q(intSessionID)).Append(",'");
            sb.Append(sql.q(DrawNum)).Append("','");
            sb.Append(sql.q(DrawConf)).Append("'");
            sql.ExecNonQuery(sb.ToString());
        }

        public static bool Flow_CheckSeedingSystem(int CampaignID, int OfferIndex)
        {
            string strSQL = "EXEC Flow_CheckSeedingSystem " + sql.q(CampaignID) + "," + sql.q(OfferIndex);
            return Convert.ToBoolean(sql.GetScalar(strSQL.ToString()));
        }

        public static DataRow Flow_DrawSeededOffer(int CampaignID, int OfferIndex)
        {
            string strSQL = "EXEC Flow_DrawSeededOffer " + sql.q(CampaignID) + "," + sql.q(OfferIndex);
            return sql.GetDataRow(strSQL);
        }

        /* -- PUBLISHER FUNCTIONS [START] -- */

        public static DataSet Publisher_GetSuppressionFiles(int PublisherIDs, int PageSize, int PageIndex, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Publisher_GetSuppressionFiles ");
            sb.Append(sql.q(PublisherIDs)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        /* -- CLIENT FUNCTIONS [START] -- */

        public static DataSet Client_GetPublishers(int ClientID, int PageSize, int PageIndex, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetPublishers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataRow Client_GetPublisherDetails(int ClientID, int PublisherID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetPublisherDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Flow_GetCampaignCodeFromSessionID(int SessionID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetCampaignCodeFromSessionID ");
            sb.Append(sql.q(SessionID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataTable Client_GetPublisherKeywords(int ClientID, int PublisherID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetPublisherKeywords ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID));

            return sql.GetDataTable(sb.ToString());
        }
        public static DataTable Flow_GetOfferData(int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetOfferData ");
            sb.Append(sql.q(CampaignID));

            return sql.GetDataTable(sb.ToString());
        }

        public static void Client_ClearPublisherKeywords(int ClientID, int PublisherID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearPublisherKeywords ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertPublisherKeyword(int ClientID, int PublisherID, int KeywordID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertPublisherKeyword ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(KeywordID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertOfferCustomFieldNames(int OfferID, string BaseName, string CustomName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertOfferCustomFieldNames ");
            sb.Append(sql.q(OfferID)).Append(", '");
            sb.Append(sql.q(BaseName)).Append("', '");
            sb.Append(sql.q(CustomName)).Append("'");
            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_UpdatePublisher(int ClientID, int PublisherID,
            bool IsActive, string Name, string Username, string Password,
            string ContactName, string WebsiteURL, string EmailAddress, string PhoneNo,
            string Address1, string Address2, string City, string Region, string Country, string PostalCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdatePublisher ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(Username)).Append("', ");
            sb.Append("'").Append(sql.q(Password)).Append("', ");
            sb.Append("'").Append(sql.q(ContactName)).Append("', ");
            sb.Append("'").Append(sql.q(WebsiteURL)).Append("', ");
            sb.Append("'").Append(sql.q(EmailAddress)).Append("', ");
            sb.Append("'").Append(sql.q(PhoneNo)).Append("', ");
            sb.Append("'").Append(sql.q(Address1)).Append("', ");
            sb.Append("'").Append(sql.q(Address2)).Append("', ");
            sb.Append("'").Append(sql.q(City)).Append("', ");
            sb.Append("'").Append(sql.q(Region)).Append("', ");
            sb.Append("'").Append(sql.q(Country)).Append("', ");
            sb.Append("'").Append(sql.q(PostalCode)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static int Client_CreatePublisher(int ClientID,
            bool IsActive, string Name, string Username, string Password,
            string ContactName, string WebsiteURL, string EmailAddress, string PhoneNo,
            string Address1, string Address2, string City, string Region, string Country, string PostalCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreatePublisher ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(Username)).Append("', ");
            sb.Append("'").Append(sql.q(Password)).Append("', ");
            sb.Append("'").Append(sql.q(ContactName)).Append("', ");
            sb.Append("'").Append(sql.q(WebsiteURL)).Append("', ");
            sb.Append("'").Append(sql.q(EmailAddress)).Append("', ");
            sb.Append("'").Append(sql.q(PhoneNo)).Append("', ");
            sb.Append("'").Append(sql.q(Address1)).Append("', ");
            sb.Append("'").Append(sql.q(Address2)).Append("', ");
            sb.Append("'").Append(sql.q(City)).Append("', ");
            sb.Append("'").Append(sql.q(Region)).Append("', ");
            sb.Append("'").Append(sql.q(Country)).Append("', ");
            sb.Append("'").Append(sql.q(PostalCode)).Append("'");

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static DataSet Client_GetAdvertisers(int ClientID, int PageSize, int PageIndex, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAdvertisers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataRow Client_GetAdvertiserDetails(int ClientID, int PublisherID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAdvertiserDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID));

            return sql.GetDataRow(sb.ToString());
        }

        public static void Client_UpdateAdvertiser(int ClientID, int AdvertiserID,
            bool IsActive, string Name,
            string ContactName, string WebsiteURL, string EmailAddress, string PhoneNo,
            string Address1, string Address2, string City, string Region, string Country, string PostalCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateAdvertiser ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(AdvertiserID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(ContactName)).Append("', ");
            sb.Append("'").Append(sql.q(WebsiteURL)).Append("', ");
            sb.Append("'").Append(sql.q(EmailAddress)).Append("', ");
            sb.Append("'").Append(sql.q(PhoneNo)).Append("', ");
            sb.Append("'").Append(sql.q(Address1)).Append("', ");
            sb.Append("'").Append(sql.q(Address2)).Append("', ");
            sb.Append("'").Append(sql.q(City)).Append("', ");
            sb.Append("'").Append(sql.q(Region)).Append("', ");
            sb.Append("'").Append(sql.q(Country)).Append("', ");
            sb.Append("'").Append(sql.q(PostalCode)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static int Client_CreateAdvertiser(int ClientID,
            bool IsActive, string Name,
            string ContactName, string WebsiteURL, string EmailAddress, string PhoneNo,
            string Address1, string Address2, string City, string Region, string Country, string PostalCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateAdvertiser ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(ContactName)).Append("', ");
            sb.Append("'").Append(sql.q(WebsiteURL)).Append("', ");
            sb.Append("'").Append(sql.q(EmailAddress)).Append("', ");
            sb.Append("'").Append(sql.q(PhoneNo)).Append("', ");
            sb.Append("'").Append(sql.q(Address1)).Append("', ");
            sb.Append("'").Append(sql.q(Address2)).Append("', ");
            sb.Append("'").Append(sql.q(City)).Append("', ");
            sb.Append("'").Append(sql.q(Region)).Append("', ");
            sb.Append("'").Append(sql.q(Country)).Append("', ");
            sb.Append("'").Append(sql.q(PostalCode)).Append("'");

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static int Client_CreateCampaign(int ClientID,
            string Name,
            int WebsiteID, int PublisherID, string Code,
            int FinalPageOfferID, int StyleID, float PayRate, int ScrubPct,
            string ImpressionURL, string ConversionURL, bool IsDynamic)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateCampaign ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append(sql.q(WebsiteID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append("'").Append(sql.q(Code)).Append("', ");
            sb.Append(sql.q(FinalPageOfferID)).Append(", ");
            sb.Append(sql.q(StyleID)).Append(", ");
            sb.Append(sql.q(PayRate)).Append(", ");
            sb.Append(sql.q(ScrubPct)).Append(", ");
            sb.Append("'").Append(sql.q(ImpressionURL)).Append("', ");
            sb.Append("'").Append(sql.q(ConversionURL)).Append("', ");
            if (IsDynamic)
            {
                sb.Append(sql.q("1"));
            }
            else
            {
                sb.Append(sql.q("0"));
            }
            Debug.WriteLine(sb.ToString());
            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Client_UpdateCampaign(int ClientID, int CampaignID,
            string Name,
            int WebsiteID, int PublisherID,
            int FinalPageOfferID, int StyleID, float PayRate, int ScrubPct,
            string ImpressionURL, string ConversionURL, bool IsDynamic)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateCampaign ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append(sql.q(WebsiteID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(FinalPageOfferID)).Append(", ");
            sb.Append(sql.q(StyleID)).Append(", ");
            sb.Append(sql.q(PayRate)).Append(", ");
            sb.Append(sql.q(ScrubPct)).Append(", ");
            sb.Append("'").Append(sql.q(ImpressionURL)).Append("', ");
            sb.Append("'").Append(sql.q(ConversionURL)).Append("', ");
            if (IsDynamic)
            {
                sb.Append(sql.q("1"));
            }
            else
            {
                sb.Append(sql.q("0"));
            }
            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_UpdateCampaignStatus(int ClientID, int CampaignID, bool IsActive)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateCampaignStatus ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID)).Append(", ");
            sb.Append(sql.q(IsActive));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_ClearCampaignFlows(int ClientID, int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearCampaignFlows ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_ClearCampaignLandingPages(int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearCampaignLandingPages ");
            sb.Append(sql.q(CampaignID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertCampaignFlow(int ClientID, int CampaignID, int FlowID, short Weight)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertCampaignFlow ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID)).Append(", ");
            sb.Append(sql.q(FlowID)).Append(", ");
            sb.Append(sql.q(Weight));

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataSet Client_GetOffers(int ClientID, int PageSize, int PageIndex, int AdvertiserID, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOffers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(AdvertiserID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataTable Client_GetOfferFieldNames(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOfferFieldNames ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataRow Client_GetOfferDetails(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOfferDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataSet Client_GetFlows(int ClientID, int PageSize, int PageIndex, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetFlows ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataSet Client_GetSurveyPages(int ClientID, int PageSize, int PageIndex, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetSurveypages ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataRow Client_GetFlowDetails(int ClientID, int FlowID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetFlowDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FlowID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Client_GetSurveyPageDetails(int ClientID, int SurveyID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetSurveyPageDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(SurveyID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Client_GetSurveyPageType(int SurveyPageID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetSurveyPageType ");
            sb.Append(sql.q(SurveyPageID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataTable Client_GetSurveyQuestions(int ClientID, int SurveyID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetSurveyQuestions ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(SurveyID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetFlowOffers(int ClientID, int FlowID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetFlowOffers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FlowID));

            return sql.GetDataTable(sb.ToString());
        }

        public static int Client_CreateFlow(int ClientID, bool IsActive, string Name, bool IsDynamic)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateFlow ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append(sql.q(IsDynamic));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static int Client_CreateSurvey(int ClientID, bool IsActive, string Name, string SurveyPageType)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateSurvey ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(SurveyPageType)).Append("'");

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Client_UpdateFlow(int ClientID, int FlowID, bool IsActive, string Name, bool IsDynamic)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateFlow ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FlowID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append(sql.q(IsDynamic));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_UpdateSurvey(int ClientID, int SurveyID, bool IsActive, string Name, string SurveyPageType)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateSurvey ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(SurveyID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(SurveyPageType)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_ClearFlowOffers(int ClientID, int FlowID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearFlowOffers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FlowID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_ClearSurveyQuestions(int ClientID, int SurveyID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearSurveyQuestions ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(SurveyID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertFlowOffer(int ClientID, int FlowID, int OfferID, int OrderPosition, int PageType)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertFlowOffer ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FlowID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(OrderPosition)).Append(",");
            sb.Append(sql.q(PageType));
            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertCampaignLandingPage(int CampaignID, int @OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertCampaignLandingPage ");
            sb.Append(sql.q(CampaignID)).Append(",");
            sb.Append(sql.q(@OfferID));
            Debug.WriteLine(sb);
            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertSurveyQuestion(int ClientID, int SurveyID, int OfferID, int OrderPosition)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertSurveyQuestion ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(SurveyID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(OrderPosition));

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataSet Client_GetWebsites(int ClientID, int PageSize, int PageIndex, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetWebsites ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataRow Client_GetWebsiteDetails(int ClientID, int WebsiteID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetWebsiteDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(WebsiteID));

            return sql.GetDataRow(sb.ToString());
        }

        public static int Client_CreateWebsite(int ClientID, bool IsActive, string Name, string DomainName, int DefaultCampaignID, string SuppressionFile)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateWebsite ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(DomainName)).Append("',");
            sb.Append(sql.q(DefaultCampaignID)).Append(", ");
            sb.Append("'").Append(sql.q(SuppressionFile)).Append("'");
            Debug.WriteLine(sb);
            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Client_UpdateWebsite(int ClientID, int WebsiteID, bool IsActive, string Name, string DomainName, int DefaultCampaignID, string SuppressionFile)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateWebsite ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(WebsiteID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(DomainName)).Append("', ");
            sb.Append(sql.q(DefaultCampaignID)).Append(", ");
            sb.Append("'").Append(sql.q(SuppressionFile)).Append("'");
            Debug.WriteLine(sb);
            sql.ExecNonQuery(sb.ToString());
        }

        public static DataSet Client_GetCampaigns(int ClientID, int PageSize, int PageIndex, int WebsiteID, int PublisherID, int FlowID, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetCampaigns ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(WebsiteID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(FlowID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataSet Client_GetCampaigns_AffSystem(int PageSize, int PageIndex, int WebsiteID, int PublisherID, int FlowID, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetCampaigns_AffSystem ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(WebsiteID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(FlowID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");
            Debug.WriteLine(sb);
            return sql.GetDataSet(sb.ToString());
        }


        public static DataRow Client_GetCampaignDetails(int ClientID, int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetCampaignDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataTable Client_GetCampaignFlows(int ClientID, int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetCampaignFlows ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetCampaignLandingPages(int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetCampaignLandingPages ");
            sb.Append(sql.q(CampaignID));

            return sql.GetDataTable(sb.ToString());
        }

        public static int Client_GetCampaignFlowCount(int ClientID, int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetCampaignFlowCount ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static DataTable Client_GetCampaignReferrers(int ClientID, int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetCampaignReferrers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetAvailableCampaigns(int ClientID, int WebsiteID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAvailableCampaigns ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(WebsiteID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetAvailableFlows(int ClientID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAvailableFlows ");
            sb.Append(sql.q(ClientID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetAvailableLandingPages(int ClientID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAvailableLandingPages ");
            sb.Append(sql.q(ClientID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetMatchingOffers(int ClientID, string Text)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetMatchingOffers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(Text)).Append("'");

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetAvailableQuestions(int ClientID, OfferUsage Usage)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAvailableQuestions ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q((int)Usage));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetAvailableOffers(int ClientID, OfferUsage Usage)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAvailableOffers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q((int)Usage));
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetAvailableFields()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetAvailableFields ");

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetAllOffers(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllOffers " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetOffersForFeeds(int ClientID)
        {
            string strSQL = "EXEC Client_GetOffersForFeeds " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAvailablePages(int ClientID)
        {
            string strSQL = "EXEC Client_GetAvailablePages " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAllWebsites(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllWebsites " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAllWebsitesWithCampaigns(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllWebsitesWithCampaigns " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAllCampaigns(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllCampaigns " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAllPublishers(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllPublishers " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAllPublishersWithCampaigns(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllPublishersWithCampaigns " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAllPublishersWithReferrers(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllPublishersWithReferrers " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetOfferCategories(int _OfferID)
        {
            string strSQL = "EXEC Client_GetOfferCategoriesByOfferID " + _OfferID.ToString();

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetOfferCategories()
        {
            string strSQL = "EXEC Client_GetOfferCategories ";
            return sql.GetDataTable(strSQL);
        }

        public static void Client_SetOfferCategories(int _OfferID, string _CategoryDetailsIDs)
        {
            string strSQL = "EXEC Client_SetOfferCategories " + _OfferID.ToString() + ", '" + sql.q(_CategoryDetailsIDs) + "'";
            sql.ExecNonQuery(strSQL);
        }

        public static DataTable Client_GetAllAdvertisers(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllAdvertisers " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_GetAllAdvertisersWithOffers(int ClientID)
        {
            string strSQL = "EXEC Client_GetAllAdvertisersWithOffers " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static void Client_ClearOfferKeywords(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearOfferKeywords ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertOfferKeyword(int ClientID, int OfferID, int KeywordID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertOfferKeyword ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(KeywordID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_ClearOfferPages(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearOfferPages ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertOfferPage(int ClientID, int OfferID, int PageID, int OrderPosition)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertOfferPage ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(PageID)).Append(", ");
            sb.Append(sql.q(OrderPosition));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_ClearOfferConditions(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearOfferConditions ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertOfferCondition(int ClientID, int OfferID, string Field, string Operator, string Value)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertOfferCondition ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append("'").Append(sql.q(Field)).Append("', ");
            sb.Append("'").Append(sql.q(Operator)).Append("', ");
            sb.Append("'").Append(sql.q(Value)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertOfferOptIn(int OfferID, string OfferText, string OfferFields)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertOfferOptIn ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append("'").Append(sql.q(OfferText)).Append("', ");
            sb.Append("'").Append(sql.q(OfferFields)).Append("'");
            Debug.WriteLine("query:" + sb);
            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertStaticPage(int OfferID, string FileName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertStaticPage ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append("'").Append(sql.q(FileName)).Append("'");
            Debug.WriteLine(sb.ToString());
            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_DeleteOptIn(string OptInID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_DeleteOptIn ");
            sb.Append(sql.q(OptInID));
            Debug.WriteLine("query:" + sb);
            sql.ExecNonQuery(sb.ToString());
        }


        public static DataTable Client_GetOfferPages(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOfferPages ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataRow Client_GetStaticFileName(int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetStaticFileName ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataTable Client_GetOfferOptIns(int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOfferOptIns ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetOfferOptInFields(int OptInID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOfferOptInFields ");
            sb.Append(sql.q(OptInID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetOfferKeywords(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOfferKeywords ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Client_GetOfferConditions(int ClientID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetOfferConditions ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataTable(sb.ToString());
        }

        public static int Client_CreateOffer(int ClientID,
            bool IsActive, string Name, int AdvertiserID,
            byte Type, byte Usage, string ExternalURL, string Title, string LinkoutImagePath, string LinkoutText,
            int RevenueTerms, float CPA, float CPM, string InsertionOrder, int LinkoutType)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateOffer ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append(sql.q(AdvertiserID)).Append(", ");
            sb.Append(sql.q(Type)).Append(", ");
            sb.Append(sql.q(Usage)).Append(", ");
            sb.Append("'").Append(sql.q(ExternalURL)).Append("', ");
            sb.Append("'").Append(sql.q(Title)).Append("', ");
            sb.Append("'").Append(sql.q(LinkoutImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(LinkoutText)).Append("', ");
            sb.Append(sql.q(RevenueTerms)).Append(", ");
            sb.Append(sql.q(CPA)).Append(", ");
            sb.Append(sql.q(CPM)).Append(", ");
            sb.Append("'").Append(sql.q(InsertionOrder)).Append("'").Append(", ");
            sb.Append(sql.q(LinkoutType));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static int Client_UpdateOffer(int ClientID, int OfferID,
            bool IsActive, string Name, int AdvertiserID,
            byte Type, byte Usage, string ExternalURL, string Title, string LinkoutImagePath, string LinkoutText,
            int RevenueTerms, float CPA, float CPM, string InsertionOrder, int LinkoutType)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateOffer ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append(sql.q(AdvertiserID)).Append(", ");
            sb.Append(sql.q(Type)).Append(", ");
            sb.Append(sql.q(Usage)).Append(", ");
            sb.Append("'").Append(sql.q(ExternalURL)).Append("', ");
            sb.Append("'").Append(sql.q(Title)).Append("', ");
            sb.Append("'").Append(sql.q(LinkoutImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(LinkoutText)).Append("', ");
            sb.Append(sql.q(RevenueTerms)).Append(", ");
            sb.Append(sql.q(CPA)).Append(", ");
            sb.Append(sql.q(CPM)).Append(", ");
            sb.Append("'").Append(sql.q(InsertionOrder)).Append("'").Append(", ");
            sb.Append(sql.q(LinkoutType));
            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }



        public static void Client_UpdateOfferField(int ClientID, int OfferID, string BaseName, string CustomName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateOfferField ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append("'").Append(sql.q(BaseName)).Append("', ");
            sb.Append("'").Append(sql.q(CustomName)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_FlagFlowAsReadOnly(int ClientID, int FlowID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_FlagFlowAsReadOnly ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FlowID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_FlagCampaignAsReadOnly(int ClientID, int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_FlagCampaignAsReadOnly ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(CampaignID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataRow Client_GetStyleDetails(int ClientID, int StyleID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetStyleDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(StyleID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataSet Client_GetStyles(int ClientID, int PageSize, int PageIndex, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetStyles ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static int Client_CreateStyle(int ClientID,
            string Name, string BackgroundColor, int HeaderWidth,
            string HeaderImagePath, string SkipButtonImagePath, string YesButtonImagePath, string NoButtonImagePath)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateStyle ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(BackgroundColor)).Append("', ");
            sb.Append(sql.q(HeaderWidth)).Append(", ");
            sb.Append("'").Append(sql.q(HeaderImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(SkipButtonImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(YesButtonImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(NoButtonImagePath)).Append("'");

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Client_UpdateStyle(int ClientID, int StyleID,
            string Name, string BackgroundColor, int HeaderWidth,
            string HeaderImagePath, string SkipButtonImagePath, string YesButtonImagePath, string NoButtonImagePath)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateStyle ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(StyleID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(BackgroundColor)).Append("', ");
            sb.Append(sql.q(HeaderWidth)).Append(", ");
            sb.Append("'").Append(sql.q(HeaderImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(SkipButtonImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(YesButtonImagePath)).Append("', ");
            sb.Append("'").Append(sql.q(NoButtonImagePath)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataSet Client_GetClientUsers(int ClientID, int PageSize, int PageIndex, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetClientUsers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataRow Client_GetClientUserDetails(int ClientID, int ClientUserID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetClientUserDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ClientUserID));

            return sql.GetDataRow(sb.ToString());
        }

        public static int Client_CreateClientUser(int ClientID,
            string Username, string Password, bool P_Account, bool P_Manage, bool P_Reports)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateClientUser ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(Username)).Append("', ");
            sb.Append("'").Append(sql.q(Password)).Append("', ");
            sb.Append(sql.q(P_Account)).Append(", ");
            sb.Append(sql.q(P_Manage)).Append(", ");
            sb.Append(sql.q(P_Reports));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Client_UpdateClientUser(int ClientID, int ClientUserID,
            string Username, string Password, bool P_Account, bool P_Manage, bool P_Reports)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateClientUser ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ClientUserID)).Append(", ");
            sb.Append("'").Append(sql.q(Username)).Append("', ");
            sb.Append("'").Append(sql.q(Password)).Append("', ");
            sb.Append(sql.q(P_Account)).Append(", ");
            sb.Append(sql.q(P_Manage)).Append(", ");
            sb.Append(sql.q(P_Reports));

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataSet Client_GetReferrers(int ClientID, int PageSize, int PageIndex, int PublisherID, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetReferrers ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataRow Client_GetReferrerDetails(int ClientID, int ReferrerID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetReferrerDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ReferrerID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataTable Client_GetReferrerKeywords(int ClientID, int ReferrerID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetReferrerKeywords ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ReferrerID));

            return sql.GetDataTable(sb.ToString());
        }

        public static void Client_ClearReferrerKeywords(int ClientID, int ReferrerID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_ClearReferrerKeywords ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ReferrerID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Client_InsertReferrerKeyword(int ClientID, int ReferrerID, int KeywordID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_InsertReferrerKeyword ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ReferrerID)).Append(", ");
            sb.Append(sql.q(KeywordID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static int Client_CreateReferrer(int ClientID,
            int PublisherID, string Name, string Code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateReferrer ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(Code)).Append("'");

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Client_UpdateReferrer(int ClientID, int ReferrerID,
            int PublisherID, string Name, string Code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateReferrer ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ReferrerID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(Code)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataSet Client_GetFeeds(int ClientID, int PageSize, int PageIndex, int IsActive, string OrderBy)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetFeeds ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PageSize)).Append(", ");
            sb.Append(sql.q(PageIndex)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append("'").Append(sql.q(OrderBy)).Append("'");

            return sql.GetDataSet(sb.ToString());
        }

        public static DataRow Client_GetFeedDetails(int ClientID, int FeedID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_GetFeedDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FeedID));

            return sql.GetDataRow(sb.ToString());
        }

        public static int Client_CreateFeed(int ClientID, bool IsActive, int OfferID, string Name, string Notes,
            FeedDeliveryMethod DeliveryMethod, string DeliveryURL, string DeliveryTemplate,
            string SuccessTokenRegex, FeedScheduleType ScheduleType, byte ScheduledDays,
            ushort ScheduledTime, ushort ScheduledInterval, FeedFileFormat FileFormat, string FileName,
            string EmailFromAddress, string EmailToAddress, string EmailSubject,
            string FTPHost, ushort FTPPort, string FTPUsername, string FTPPassword, string FTPTargetPath)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_CreateFeed ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(Notes)).Append("', ");
            sb.Append(sql.q((int)DeliveryMethod)).Append(", ");
            sb.Append("'").Append(sql.q(DeliveryURL)).Append("', ");
            sb.Append("'").Append(sql.q(DeliveryTemplate)).Append("', ");
            sb.Append("'").Append(sql.q(SuccessTokenRegex)).Append("', ");
            sb.Append(sql.q((int)ScheduleType)).Append(", ");
            sb.Append(sql.q(ScheduledDays)).Append(", ");
            sb.Append(sql.q(ScheduledTime)).Append(", ");
            sb.Append(sql.q(ScheduledInterval)).Append(", ");
            sb.Append(sql.q((int)FileFormat)).Append(", ");
            sb.Append("'").Append(sql.q(FileName)).Append("', ");
            sb.Append("'").Append(sql.q(EmailFromAddress)).Append("', ");
            sb.Append("'").Append(sql.q(EmailToAddress)).Append("', ");
            sb.Append("'").Append(sql.q(EmailSubject)).Append("', ");
            sb.Append("'").Append(sql.q(FTPHost)).Append("', ");
            sb.Append(sql.q(FTPPort)).Append(", ");
            sb.Append("'").Append(sql.q(FTPUsername)).Append("', ");
            sb.Append("'").Append(sql.q(FTPPassword)).Append("', ");
            sb.Append("'").Append(sql.q(FTPTargetPath)).Append("'");

            //HttpContext.Current.Response.Write(sb.ToString());
            //HttpContext.Current.Response.End();

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static int Client_UpdateFeed(int ClientID, int FeedID, bool IsActive, int OfferID, string Name, string Notes,
            FeedDeliveryMethod DeliveryMethod, string DeliveryURL, string DeliveryTemplate,
            string SuccessTokenRegex, FeedScheduleType ScheduleType, byte ScheduledDays,
            ushort ScheduledTime, ushort ScheduledInterval, FeedFileFormat FileFormat, string FileName,
            string EmailFromAddress, string EmailToAddress, string EmailSubject,
            string FTPHost, ushort FTPPort, string FTPUsername, string FTPPassword, string FTPTargetPath)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_UpdateFeed ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FeedID)).Append(", ");
            sb.Append(sql.q(IsActive)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(Notes)).Append("', ");
            sb.Append(sql.q((int)DeliveryMethod)).Append(", ");
            sb.Append("'").Append(sql.q(DeliveryURL)).Append("', ");
            sb.Append("'").Append(sql.q(DeliveryTemplate)).Append("', ");
            sb.Append("'").Append(sql.q(SuccessTokenRegex)).Append("', ");
            sb.Append(sql.q((int)ScheduleType)).Append(", ");
            sb.Append(sql.q(ScheduledDays)).Append(", ");
            sb.Append(sql.q(ScheduledTime)).Append(", ");
            sb.Append(sql.q(ScheduledInterval)).Append(", ");
            sb.Append(sql.q((int)FileFormat)).Append(", ");
            sb.Append("'").Append(sql.q(FileName)).Append("', ");
            sb.Append("'").Append(sql.q(EmailFromAddress)).Append("', ");
            sb.Append("'").Append(sql.q(EmailToAddress)).Append("', ");
            sb.Append("'").Append(sql.q(EmailSubject)).Append("', ");
            sb.Append("'").Append(sql.q(FTPHost)).Append("', ");
            sb.Append(sql.q(FTPPort)).Append(", ");
            sb.Append("'").Append(sql.q(FTPUsername)).Append("', ");
            sb.Append("'").Append(sql.q(FTPPassword)).Append("', ");
            sb.Append("'").Append(sql.q(FTPTargetPath)).Append("'");

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static DataTable Client_GetAllLeadColumns()
        {
            return sql.GetDataTable("EXEC Client_GetAllLeadColumns");
        }

        public static bool Client_IsAdvertiserNameUnique(int ClientID, int AdvertiserIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsAdvertiserNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(AdvertiserIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsClientUsernameUnique(int ClientID, int ClientUserIDToExclude, string Username)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsClientUsernameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(ClientUserIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Username)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsFeedNameUnique(int ClientID, int FeedIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsFeedNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FeedIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsWebsiteNameUnique(int ClientID, int WebsiteIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsWebsiteNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(WebsiteIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsStyleNameUnique(int ClientID, int StyleIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsStyleNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(StyleIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsReferrerNameUnique(int ClientID, int PublisherID, int ReferrerIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsReferrerNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(ReferrerIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsReferrerCodeUnique(int ClientID, int PublisherID, int ReferrerIDToExclude, string Code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsReferrerCodeUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(ReferrerIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Code)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsCampaignNameUnique(int ClientID, int WebsiteID, int CampaignIDToExclude, string Code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsCampaignNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(WebsiteID)).Append(", ");
            sb.Append(sql.q(CampaignIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Code)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsFlowNameUnique(int ClientID, int FlowIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsFlowNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FlowIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsPublisherNameUnique(int ClientID, int PublisherIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsPublisherNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsPublisherUsernameUnique(int ClientID, int PublisherIDToExclude, string Username)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsPublisherUsernameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(PublisherIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Username)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_IsOfferNameUnique(int ClientID, int OfferIDToExclude, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_IsOfferNameUnique ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(OfferIDToExclude)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static DataTable Client_GetKeywordSuggestions(string Text)
        {
            string strSQL = "EXEC Client_GetKeywordSuggestions '" + sql.q(Text) + "'";
            return sql.GetDataTable(strSQL);
        }

        public static DataRow Client_LogIn(string Username, string Password)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_LogIn ");
            sb.Append("'").Append(sql.q(Username)).Append("', ");
            sb.Append("'").Append(sql.q(Password)).Append("'");

            return sql.GetDataRow(sb.ToString());
        }
        /* -- CLIENT FUNCTIONS [END] -- */


        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----

        /* -- PUBLISHER FUNCTIONS [START] -- */

        public static DataRow Publisher_LogIn(string Username, string Password)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Publisher_LogIn ");
            sb.Append("'").Append(sql.q(Username)).Append("', ");
            sb.Append("'").Append(sql.q(Password)).Append("'");

            return sql.GetDataRow(sb.ToString());
        }

        /* -- PUBLISHER FUNCTIONS [END] -- */

        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----


        /* -- PAGE BUILDER FUNCTIONS [START] -- */

        public static bool Client_PB_RenamePage(int ClientID, string GUID, string PageName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_RenamePage ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(GUID)).Append("', ");
            sb.Append("'").Append(sql.q(PageName)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_PB_DeletePage(int ClientID, string GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_DeletePage ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(GUID)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_PB_CreatePageFolder(int ClientID, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_CreatePageFolder ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");
            Debug.WriteLine(sb);
            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_PB_DeletePageFolder(int ClientID, int FolderID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_DeletePageFolder ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FolderID));

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Client_PB_RenamePageFolder(int ClientID, int FolderID, string Name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_RenamePageFolder ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FolderID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static DataTable Client_PB_GetPageFolders(int ClientID)
        {
            string strSQL = "EXEC Client_PB_GetPageFolders " + sql.q(ClientID);

            return sql.GetDataTable(strSQL);
        }

        public static DataTable Client_PB_GetPages(int ClientID, int FolderID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_GetPages ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append(sql.q(FolderID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataRow Client_PB_GetPageDetails(int ClientID, string GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_GetPageDetails ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(GUID)).Append("'");

            return sql.GetDataRow(sb.ToString());
        }

        public static bool Client_PB_SavePage(int ClientID, string GUID, int FolderID, string Name, string JSONDefinition)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Client_PB_SavePage ");
            sb.Append(sql.q(ClientID)).Append(", ");
            sb.Append("'").Append(sql.q(GUID)).Append("', ");
            sb.Append(sql.q(FolderID)).Append(", ");
            sb.Append("'").Append(sql.q(Name)).Append("', ");
            sb.Append("'").Append(sql.q(JSONDefinition)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }
        /* -- PAGE BUILDER FUNCTIONS [END] -- */


        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----


        /* -- FLOW/PUBLIC FUNCTIONS [START] -- */

        public static void RefillLeadHashtable(int campaignId, int sessionId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_RefillLeadHashtable ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(sessionId));

            RefillLeadHashtable(sb);
        }

        public static void RefillLeadHashtable(int recordId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_RefillLeadHashtableLeadID ");
            sb.Append(sql.q(recordId));

            RefillLeadHashtable(sb);
        }

        private static void RefillLeadHashtable(StringBuilder proctext)
        {
            DataRow dr = sql.GetDataRow(proctext.ToString());
            
            if (dr != null)
            {
                Hashtable lead = new Hashtable();

                lead["emailaddress"] = (string)dr["EmailAddress"];
                lead["firstname"] = (string)dr["FirstName"];
                lead["lastname"] = (string)dr["LastName"];
                lead["homephoneno"] = (string)dr["HomePhoneNo"];
                lead["postalcode"] = (string)dr["PostalCode"];
                lead["custom1"] = (string)dr["Custom1"];

                Sessions.Visitor["Lead"] = lead;
            }
            else
                Sessions.Visitor["Lead"] = new Hashtable();
        }

        public static DataRow Flow_GetDefaultCampaignDetails(int WebsiteID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetDefaultCampaignDetails ");
            sb.Append(sql.q(WebsiteID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Flow_GetCampaignDetailsByCode(string Code)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetCampaignDetailsByCode ");
            sb.Append("'").Append(sql.q(Code)).Append("'");

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Flow_GetCampaignDetailsByID(int CampaignID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetCampaignDetailsByID ");
            sb.Append(sql.q(CampaignID));

            return sql.GetDataRow(sb.ToString());
        }

        public static int Flow_InsertSession(string SessionGUID, int VisitorID, int CampaignID, string Refer, string SubID, string RawLandingURL, string UserAgent, string HTTPReferrer, string IPAddress, string LocationCode, string referidtext, bool clearSession = false)
        {
            return Flow_InsertSession(SessionGUID, VisitorID, CampaignID, Refer, SubID, RawLandingURL, UserAgent, HTTPReferrer, IPAddress, LocationCode, "", "", "", "", "", "", referidtext, clearSession);
        }

        public static int Flow_InsertSession(string SessionGUID, int VisitorID, int CampaignID, string Refer, string SubID, string RawLandingURL, string UserAgent, string HTTPReferrer, string IPAddress, string LocationCode, string ReturnPath, string strAffID, string strHitID, string strC1, string strC2, string strC3, string referidtext, bool clearSession = false)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_InsertSession ");
            sb.Append("'").Append(sql.q(SessionGUID)).Append("', ");
            sb.Append(sql.q(VisitorID)).Append(", ");
            sb.Append(sql.q(CampaignID)).Append(", ");
            sb.Append("'").Append(sql.q(Refer)).Append("', ");
            sb.Append("'").Append(sql.q(SubID)).Append("', ");
            sb.Append("'").Append(sql.q(RawLandingURL)).Append("', ");
            sb.Append("'").Append(sql.q(UserAgent)).Append("', ");
            sb.Append("'").Append(sql.q(HTTPReferrer)).Append("', ");
            sb.Append("'").Append(sql.q(IPAddress)).Append("', ");
            sb.Append("'").Append(sql.q(LocationCode)).Append("', ");
            sb.Append("'").Append(sql.q(ReturnPath)).Append("', ");
            sb.Append("'").Append(sql.q(strAffID)).Append("', ");
            sb.Append("'").Append(sql.q(strHitID)).Append("', ");
            sb.Append("'").Append(sql.q(strC1)).Append("', ");
            sb.Append("'").Append(sql.q(strC2)).Append("', ");
            sb.Append("'").Append(sql.q(strC3)).Append("', ");
            sb.Append("'").Append(sql.q(referidtext)).Append("', ");
            sb.Append("'").Append(sql.q(clearSession)).Append("'");
            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void GAPC_InsertAffiliate(string referidtext)
        {
            SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            conection.Open();

            try
            {
                StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Insert_Affiliate ");
            sb.Append("'").Append(sql.q(referidtext)).Append("'");

            sql.ExecNonQuery(sb.ToString(), conection);
            }
            catch( Exception ex){}
            finally
            {
            conection.Dispose();
            }
        }

        public static int Flow_InsertVisitor(int campaignId, int lpOfferId, string GUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_InsertVisitor ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append("'").Append(sql.q(GUID)).Append("'");

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static bool Flow_IsVisitorValid(int visitorId, int campaignId, int lpOfferId, string visitorGUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_IsVisitorValid ");
            sb.Append(sql.q(visitorId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append("'").Append(sql.q(visitorGUID)).Append("'");

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static DataRow Flow_GetOfferDetails(int CampaignID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetOfferDetails ");
            sb.Append(sql.q(CampaignID)).Append(", ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Flow_GetHeaderOptional(int FlowID, int OfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetHeaderOptional ");
            sb.Append(sql.q(FlowID)).Append(", ");
            sb.Append(sql.q(OfferID));

            return sql.GetDataRow(sb.ToString());
        }

        public static int Flow_GetOfferImpressionCount(int OfferID)
        {
            string strSQL = "EXEC Flow_GetOfferImpressionCount " + sql.q(OfferID);
            return Convert.ToInt32(sql.GetScalar(strSQL));
        }

        public static DataRow Flow_GetPageDetails(int PageID)
        {
            string strSQL = "EXEC Flow_GetPageDetails " + sql.q(PageID);
            return sql.GetDataRow(strSQL);
        }

        public static DataTable Flow_GetOfferOptIn(int SurveyPageID)
        {
            string strSQL = "EXEC Flow_GetOfferOptIn " + sql.q(SurveyPageID);
            return sql.GetDataTable(strSQL);
        }

        public static int Flow_GetOfferPageID(int OfferID, int OrderPosition)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetOfferPageID ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(OrderPosition));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static int Flow_GetSurveyPageID(int FlowID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_GetSurveyPageID ");
            sb.Append(sql.q(FlowID));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static int Flow_GetOfferPageCount(int OfferID)
        {
            string strSQL = "EXEC Flow_GetOfferPageCount " + sql.q(OfferID);

            return Convert.ToInt32(sql.GetScalar(strSQL));
        }

        public static int Flow_SaveLead(int OfferImpressionID, Hashtable Lead)
        {
            try
            {
            if (OfferImpressionID == 0)
            {
                Hashtable session = Sessions.Visitor;

                if (session["OfferImpressionID"] == null)
                    OfferImpressionID = 0;
                else if (!int.TryParse(Convert.ToString(session["OfferImpressionID"]), out OfferImpressionID))
                    OfferImpressionID = 0;
                if (OfferImpressionID == 0)
                {
            //        int landingPageOfferId = -1;
            //        int intSessionID;
            //        if (session["SessionID"] == null)
            //            intSessionID = 0;
            //        else if (!int.TryParse(Convert.ToString(session["SessionID"]), out intSessionID))
            //            intSessionID = 0;

            //        if (intSessionID == 0)
            //        {
            //            if (session["SessionGUID"] == null)
            //                session["SessionGUID"] = Guid.NewGuid().ToString();

            //            int campaignId = 2;

            //            if (session["VisitorID"] == null)
            //            {
            //                string visitorGUID = Utility.GetBase32UID();
            //                int intWebsiteID = ros.Flow_GetWebsiteIDByDomainName(HttpContext.Current.Request.Url.Host);
            //                if (intWebsiteID > 0)
            //                {
            //                    DataRow dr = ros.Flow_GetDefaultCampaignDetails(intWebsiteID);
            //                    if (dr != null)
            //                    {
            //                        campaignId = Convert.ToInt32(dr["CampaignID"]);
            //                        landingPageOfferId = FlowDirector.GetRandomLandingPage(campaignId);
            //                    }
            //                }

            //                int visitorId = ros.Flow_InsertVisitor(campaignId, landingPageOfferId, visitorGUID);
            //                Sessions.Visitor["VisitorID"] = visitorId;

            //            }

            //            string strReferrer = Utility.GetDefaultValue(HttpContext.Current.Request.QueryString, "r", "");
            //            string strSubID = Utility.GetDefaultValue(HttpContext.Current.Request.QueryString, "subid", "");
            //            string strUrlReferrer = HttpContext.Current.Request.UrlReferrer == null ? String.Empty : HttpContext.Current.Request.UrlReferrer.ToString();

                    ////var forwardedFor = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    ////string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
                    //string IpAddress = HttpContext.Current.Request.UserHostAddress;

            //            intSessionID = ros.Flow_InsertSession(
            //                session["SessionGUID"].ToString(), (int)session["VisitorID"],
            //                campaignId, strReferrer, strSubID,
                    //                HttpContext.Current.Request.RawUrl, HttpContext.Current.Request.UserAgent, strUrlReferrer, IpAddress,
            //                session["LocationCode"].ToString(), Functions.GetReferID());
            //        }
            //        string UserAgent = HttpContext.Current.Request.UserAgent;
            //        if (UserAgent != null && UserAgent.Contains("Mobile") == true)
            //        {
            //            session["IsMobile"] = 1;
            //        }
            //        else
            //        {
            //            session["IsMobile"] = 0;
            //        }

            //        session["SessionID"] = intSessionID;

            //        int intLPOfferImpressionID = ros.Flow_InsertOfferImpression(intSessionID, landingPageOfferId, int.MinValue, ImpressionProvider.LandingPage);
            //        session["OfferImpressionID"] = intLPOfferImpressionID;

                    ErrorHandling.SendException("SaveLead", new Exception("No OfferImpressionID Found"), String.Format("Current URL: {0}, \nNew ImpressionID: {1}", HttpContext.Current.Request.Url.AbsoluteUri, session["OfferImpressionID"]));
                }
            }
            } catch (Exception ex)
            {
                ErrorHandling.SendException("SaveLead", ex, String.Format("Current URL: {0}", HttpContext.Current.Request.Url.AbsoluteUri));
            }

            DateTime dtDOB;
            if (!DateTime.TryParse(GetLeadValue(Lead, "DateOfBirth"), out dtDOB))
                dtDOB = DateTime.MinValue;

            /*
            if (GetLeadValue(Lead, "Gender") == "" && Lead["firstname"] != "")
            {
                 
                HTTPGet req = new HTTPGet();
                req.Request("http://www.gpeters.com/names/baby-names.php?button=Go&name=" + Lead["firstname"]);
                string myResponseBody = req.ResponseBody;
                int first = myResponseBody.IndexOf("<b>It's a ");
                string GenderFound = myResponseBody.Substring(first + 10, 4);
                Debug.WriteLine("GenderString:" + GenderFound);
                if (GenderFound == "girl")
                {
                    Lead["Gender"] = "F";
                }
                if (GenderFound == "boy!")
                {
                    Lead["Gender"] = "M";
                }
                Debug.WriteLine("I had to guess a gender. It was:" + Lead["Gender"]);
            }
            */

            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_SaveLead ");
            sb.Append(sql.q(OfferImpressionID)).Append(", ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "firstname"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "MiddleName"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "LastName"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "EmailAddress"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Address"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Address2"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "City"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "State"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Country"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "PostalCode"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "HomePhoneNo"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "WorkPhoneNo"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "CellPhoneNo"))).Append("', ");
            sb.Append("'").Append(Lead["Gender"]).Append("', ");
            sb.Append(sql.q(dtDOB)).Append(", ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Occupation"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Income"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Title"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom1"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom2"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom3"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom4"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom5"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom6"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom7"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom8"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom9"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom10"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom11"))).Append("', ");
            sb.Append("'").Append(sql.q(GetLeadValue(Lead, "Custom12"))).Append("'");
            Debug.WriteLine(sb.ToString());
            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Flow_UpdateLeadNormals(int OfferImpressionID, short NormalAge, string NormalGender, int NormalIncome)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_UpdateLeadNormals ");
            sb.Append(sql.q(OfferImpressionID)).Append(", ");
            sb.Append(sql.q(NormalAge)).Append(", ");
            sb.Append("'").Append(sql.q(NormalGender)).Append("', ");
            sb.Append(sql.q(NormalIncome));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Flow_UpdateLeadField(int OfferImpressionID, string FieldName, string Value)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_UpdateLeadField ");
            sb.Append(sql.q(OfferImpressionID)).Append(", ");
            sb.Append("'").Append(sql.q(FieldName)).Append("', ");
            sb.Append("'").Append(sql.q(Value)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataTable Flow_GetCampaignFlows(int CampaignID, int SessionID)
        {
            StringBuilder sb = new StringBuilder();
            string strSQL = "EXEC Flow_GetCampaignFlows " + sql.q(CampaignID) + ", " + sql.q(SessionID);
            return sql.GetDataTable(strSQL);
        }

        public static void Flow_UpdateSession(int SessionID, int PresentedFlowID, bool DynamicFlowPresented)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_UpdateSession ");
            sb.Append(sql.q(SessionID)).Append(", ");
            sb.Append(sql.q(PresentedFlowID)).Append(", ");
            sb.Append(sql.q(DynamicFlowPresented));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Flow_UpdateSessionLP(int SessionID, int OfferImpressionID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_UpdateSessionLP ");
            sb.Append(sql.q(SessionID)).Append(", ");
            sb.Append(sql.q(OfferImpressionID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Flow_UpdateSessionFinalPage(int SessionID, int OfferImpressionID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_UpdateSessionFinalPage ");
            sb.Append(sql.q(SessionID)).Append(", ");
            sb.Append(sql.q(OfferImpressionID));

            sql.ExecNonQuery(sb.ToString());
        }

        //public static DataTable Flow_GetFlowOffers(int FlowID)
        //{
        //    string strSQL = "EXEC Flow_GetFlowOffers " + sql.q(FlowID);
        //    return sql.GetDataTable(strSQL);
        //}

        public static DataTable Flow_GetFilteredOffers(int SessionID)
        {
            string strSQL = "EXEC Flow_GetFilteredOffers_CapFailover " + sql.q(SessionID);
            return sql.GetDataTable(strSQL);
        }

        public static DataRow Flow_GetFlowDetails(int FlowID)
        {
            string strSQL = "EXEC Flow_GetFlowDetails " + sql.q(FlowID);
            return sql.GetDataRow(strSQL);
        }


        public static DataRow Flow_GetStaticFileName(int OfferID)
        {
            string strSQL = "EXEC Flow_GetStaticFileName " + sql.q(OfferID);
            return sql.GetDataRow(strSQL);
        }

        public static DataRow Flow_GetStyleDetails(int StyleID)
        {
            string strSQL = "EXEC Flow_GetStyleDetails " + sql.q(StyleID);
            return sql.GetDataRow(strSQL);
        }

        public static DataRow Flow_GetStyleDetailsByOfferID(int OfferID, int FlowID)
        {
            string strSQL = "EXEC Flow_GetStyleDetailsByOfferID " + sql.q(OfferID) + "," + sql.q(FlowID);
            return sql.GetDataRow(strSQL);
        }


        public static int Flow_InsertOfferImpression(int SessionID, int OfferID, int PreviousOfferID, ImpressionProvider Provider)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_InsertOfferImpression ");
            sb.Append(sql.q(SessionID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(PreviousOfferID)).Append(", ");
            sb.Append(sql.q((int)Provider));

            return Convert.ToInt32(sql.GetScalar(sb.ToString()));
        }

        public static void Flow_FlagOfferImpressionAsCompleted(int OfferImpressionID)
        {
            string strSQL = "EXEC Flow_FlagOfferImpressionAsCompleted " + sql.q(OfferImpressionID);
            sql.ExecNonQuery(strSQL);
        }

        public static bool Flow_CheckSurveyPageInFlow(int FlowID)
        {
            string strSQL = "EXEC Flow_CheckSurveyPageInFlow " + sql.q(FlowID);
            return Convert.ToBoolean(sql.GetScalar(strSQL.ToString()));
        }

        public static DataTable Flow_GetOfferConditions(int OfferID)
        {
            string strSQL = "EXEC Flow_GetOfferConditions " + sql.q(OfferID);
            return sql.GetDataTable(strSQL);
        }

        public static int Flow_GetWebsiteIDByDomainName(string DomainName)
        {
            string strSQL = "EXEC Flow_GetWebsiteIDByDomainName '" + sql.q(DomainName.Replace("dev2","www2.")) + "'";
            object foo = sql.GetScalar(strSQL);
            return foo == DBNull.Value ? int.MinValue : Convert.ToInt32(foo);
        }

        public static DataRow Flow_GetWebsiteInfoByDomainName(string DomainName)
        {
            string strSQL = "EXEC Flow_GetWebsiteInfoByDomainName '" + sql.q(DomainName.Replace("dev2", "www2.")) + "'";
            DataRow foo = sql.GetDataRow(strSQL);
            return foo;
        }

        public static int Flow_GetWebsiteIDByCampaign(int CampaignID)
        {
            string strSQL = "EXEC Flow_GetWebsiteIDByCampaign " + CampaignID;
            object foo = sql.GetScalar(strSQL);
            return foo == DBNull.Value ? int.MinValue : Convert.ToInt32(foo);
        }
        
        public static void Flow_FlagLinkOutAsLaunched(int SessionID, int OfferImpressionID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Flow_FlagLinkOutAsLaunched ");
            sb.Append(sql.q(SessionID)).Append(", ");
            sb.Append(sql.q(OfferImpressionID));

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataRow Flow_GetCampaignTrackingURLs(int SessionID)
        {
            string strSQL = "EXEC Flow_GetCampaignTrackingURLs " + sql.q(SessionID);
            return sql.GetDataRow(strSQL);
        }

        public static DataTable Flow_GetCampaignLandingPages(int campaignId)
        {
            string strSQL = "EXEC Flow_GetCampaignLandingPages " + sql.q(campaignId);
            return sql.GetDataTable(strSQL);
        }
        /* -- FLOW/PUBLIC FUNCTIONS [END] -- */


        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----


        /* -- ANALYTICS FUNCTIONS [START] -- */

        public static void Analytics_CacheOfferScores(SqlConnection cn, int SessionID, int OfferID,
            float DemographicAgeScore, float DemographicGenderScore, float DemographicIncomeScore,
            float GeographicScore, float KeywordMatchScore, float SequentialScore, float BehaviouralScore)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Analytics_CacheOfferScores ");
            sb.Append(sql.q(SessionID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(DemographicAgeScore)).Append(", ");
            sb.Append(sql.q(DemographicGenderScore)).Append(", ");
            sb.Append(sql.q(DemographicIncomeScore)).Append(", ");
            sb.Append(sql.q(GeographicScore)).Append(", ");
            sb.Append(sql.q(KeywordMatchScore)).Append(", ");
            sb.Append(sql.q(SequentialScore)).Append(", ");
            sb.Append(sql.q(BehaviouralScore)).Append(" ");

            sql.ExecNonQuery(sb.ToString(), cn);
        }

        public static DataTable Analytics_GetCachedScores(int SessionID)
        {
            string strSQL = "EXEC Analytics_GetCachedScores " + sql.q(SessionID);
            return sql.GetDataTable(strSQL);
        }

        public static DataRow Analytics_GetDemographicScores(SqlConnection Connection, int OfferID, int NormalAge, string NormalGender, int NormalIncome)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Analytics_GetDemographicScores ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(NormalAge)).Append(", ");
            sb.Append("'").Append(sql.q(NormalGender)).Append("', ");
            sb.Append(sql.q(NormalIncome));

            return sql.GetDataRow(sb.ToString(), Connection);
        }

        public static DataRow Analytics_GetGeographicScore(SqlConnection Connection, int OfferID, string LocationCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Analytics_GetGeographicScore ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append("'").Append(sql.q(LocationCode)).Append("'");

            return sql.GetDataRow(sb.ToString(), Connection);
        }

        public static DataRow Analytics_GetBehaviouralScore(SqlConnection Connection, int OfferID, int PreviousOfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Analytics_GetBehaviouralScore ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(PreviousOfferID));

            return sql.GetDataRow(sb.ToString(), Connection);
        }

        public static float Analytics_GetSequentialScore(SqlConnection Connection, int OfferID, int PreviousOfferID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Analytics_GetSequentialScore ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(PreviousOfferID));

            return Convert.ToSingle(sql.GetScalar(sb.ToString(), Connection));
        }

        public static void Analytics_StoreTotalScore(SqlConnection Connection, int SessionID, int OfferID, float AbsTotalScore)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Analytics_StoreTotalScore ");
            sb.Append(sql.q(SessionID)).Append(", ");
            sb.Append(sql.q(OfferID)).Append(", ");
            sb.Append(sql.q(AbsTotalScore));

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataTable Analytics_GetReferrerKeywords(SqlConnection Connection, int SessionID)
        {
            string strSQL = "EXEC Analytics_GetReferrerKeywords " + sql.q(SessionID);
            return sql.GetDataTable(strSQL, Connection);
        }

        public static DataTable Analytics_GetOfferKeywords(SqlConnection Connection, int OfferID)
        {
            string strSQL = "EXEC Analytics_GetOfferKeywords " + sql.q(OfferID);
            return sql.GetDataTable(strSQL, Connection);
        }

        public static short Analytics_GetNormalizedAge(short Age)
        {
            string strSQL = "EXEC Analytics_GetNormalizedAge " + sql.q(Age);
            return Convert.ToByte(sql.GetScalar(strSQL));
        }

        public static int Analytics_GetNormalizedIncome(int Income)
        {
            string strSQL = "EXEC Analytics_GetNormalizedIncome " + sql.q(Income);
            return Convert.ToInt32(sql.GetScalar(strSQL));
        }
        /* -- ANALYTICS FUNCTIONS [END] -- */


        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----


        /* -- FEED FUNCTIONS [START] -- */
        public static DataTable Feed_GetPendingTasks(SqlConnection cn)
        {
            return sql.GetDataTable("EXEC Feed_GetPendingTasks", cn);
        }

        public static int Feed_CreateRun(SqlConnection cn, int FeedID)
        {
            string strSQL = "EXEC Feed_CreateRun " + sql.q(FeedID);
            return Convert.ToInt32(sql.GetScalar(strSQL, cn));
        }

        public static void Feed_CompleteRun(SqlConnection cn, int RunID, FeedTaskStatus Status, string ErrorMessage)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Feed_CompleteRun ");
            sb.Append(sql.q(RunID)).Append(", ");
            sb.Append(sql.q((int)Status)).Append(", ");
            sb.Append("'").Append(sql.q(ErrorMessage)).Append("'");

            sql.ExecNonQuery(sb.ToString(), cn);
        }

        public static void Feed_UpdateRunLead(int FeedRunLeadID, FeedLeadStatus Status, string DataString)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Feed_UpdateRunLead ");
            sb.Append(sql.q(FeedRunLeadID)).Append(", ");
            sb.Append(sql.q((int)Status)).Append(", ");
            sb.Append("'").Append(sql.q(DataString)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Feed_CompleteRunLead(int FeedRunLeadID, FeedLeadStatus Status, string Response, string SuccessToken)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Feed_CompleteRunLead ");
            sb.Append(sql.q(FeedRunLeadID)).Append(", ");
            sb.Append(sql.q((int)Status)).Append(", ");
            sb.Append("'").Append(sql.q(Response)).Append("', ");
            sb.Append("'").Append(sql.q(SuccessToken)).Append("'");

            sql.ExecNonQuery(sb.ToString());
        }

        public static DataRow Feed_GetOfferImpressionDetails(SqlConnection cn, int OfferImpressionID)
        {
            string strSQL = "EXEC Feed_GetOfferImpressionDetails " + sql.q(OfferImpressionID);
            return sql.GetDataRow(strSQL, cn);
        }

        public static DataTable Feed_GetApplicableFeeds(SqlConnection cn, int OfferID)
        {
            string strSQL = "EXEC Feed_GetApplicableFeeds " + sql.q(OfferID);
            return sql.GetDataTable(strSQL, cn);
        }

        public static void Feed_QueueLead(SqlConnection cn, int FeedID, int LeadID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Feed_QueueLead ")
                .Append(sql.q(FeedID)).Append(", ")
                .Append(sql.q(LeadID));

            sql.ExecNonQuery(sb.ToString(), cn);
        }

        public static void Feed_MoveQueuedLeadsToRun(SqlConnection cn, int FeedID, int RunID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Feed_MoveQueuedLeadsToRun ")
                .Append(sql.q(FeedID)).Append(", ")
                .Append(sql.q(RunID));

            sql.ExecNonQuery(sb.ToString(), cn);
        }

        public static DataTable Feed_GetRunLeads(SqlConnection cn, int RunID)
        {
            string strSQL = "EXEC Feed_GetRunLeads " + sql.q(RunID);
            return sql.GetDataTable(strSQL, cn);
        }

        public static void Feed_UpdateFeedLastRunTime(SqlConnection cn, int FeedID)
        {
            string strSQL = "EXEC Feed_UpdateFeedLastRunTime " + sql.q(FeedID);
            sql.ExecNonQuery(strSQL, cn);
        }

        /* -- FEED FUNCTIONS [END] -- */


        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----


        /* -- REPORTING FUNCTIONS [START] -- */
        public static DataTable Reports_GetAdvertisers(int clientId)
        {
            string strSQL = "EXEC Reports_GetAdvertisers " + sql.q(clientId);
            return sql.GetDataTable(strSQL);
        }

        public static DataTable Reports_GetCampaigns(int clientId, int websiteId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetCampaigns ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId));
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrersReport(int clientId, int websiteId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrersReport ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId));
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetAffIDs(int clientId, int websiteId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetAffIDs ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId));
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetSubIDs(int clientId, int websiteId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetSubIDs ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId));
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetCampaigns_AffSystem(int publisherid, int websiteId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetCampaigns_AffSystem ");
            sb.Append(sql.q(publisherid)).Append(", ");
            sb.Append(sql.q(websiteId));
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetOffers(int clientId, int advertiserId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetOffers ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(advertiserId));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetPublishers(int clientId)
        {
            string strSQL = "EXEC Reports_GetPublishers " + sql.q(clientId);
            return sql.GetDataTable(strSQL);
        }

        public static DataTable Reports_GetPublishers_AffSystem(int clientId)
        {
            string strSQL = "EXEC Reports_GetPublishers_AffSystem " + sql.q(clientId);
            return sql.GetDataTable(strSQL);
        }

        public static DataTable Reports_GetWebsites(int clientId, int publisherId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetWebsites ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(publisherId));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetWebsites_AffSystem(int publisherId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetWebsites_AffSystem ");
            sb.Append(sql.q(publisherId));
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }

        // -- general report -- //
        //public static DataTable Reports_GetGeneralReportByWebsite(int clientId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetGeneralReportByWebsite ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    //sb.Append(sql.q(websiteId)).Append(", ");
        //    //sb.Append(sql.q(campaignId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}

        //public static DataTable Reports_GetGeneralReportByDate(int clientId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetGeneralReportByDate ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    //sb.Append(sql.q(websiteId)).Append(", ");
        //    //sb.Append(sql.q(campaignId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}

        //public static DataTable Reports_GetGeneralReportByCampaign(int clientId, int websiteId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetGeneralReportByCampaign ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    sb.Append(sql.q(websiteId)).Append(", ");
        //    //sb.Append(sql.q(campaignId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}

        //public static DataTable Reports_GetGeneralReportByLandingPage(int clientId, int campaignId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetGeneralReportByLandingPage ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    sb.Append(sql.q(campaignId)).Append(", ");
        //    //sb.Append(sql.q(campaignId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}

        //// -- publisher report -- //
        //public static DataTable Reports_GetPublisherReportByDate(int clientId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetPublisherReportByDate ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    //sb.Append(sql.q(websiteId)).Append(", ");
        //    //sb.Append(sql.q(campaignId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}

        //public static DataTable Reports_GetPublisherReportByPublisher(int clientId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetPublisherReportByPublisher ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    //sb.Append(sql.q(websiteId)).Append(", ");
        //    //sb.Append(sql.q(campaignId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}

        //public static DataTable Reports_GetPublisherReportByWebsite(int clientId, int publisherId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetPublisherReportByWebsite ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    sb.Append(sql.q(publisherId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}

        //public static DataTable Reports_GetPublisherReportByCampaign(int clientId, int publisherId, int websiteId, DateTime startDate, DateTime endDate)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("EXEC Reports_GetPublisherReportByCampaign ");
        //    sb.Append(sql.q(clientId)).Append(", ");
        //    sb.Append(sql.q(publisherId)).Append(", ");
        //    sb.Append(sql.q(websiteId)).Append(", ");
        //    sb.Append(sql.q(startDate)).Append(", ");
        //    sb.Append(sql.q(endDate));

        //    return sql.GetDataTable(sb.ToString());
        //}




        // combined stats reports
        public static DataTable Reports_GetGeneralStatsByDate(int clientId, int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByDate ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetGeneralStatsByDate_AffSystem(int websiteId, int publisherId, String SelMonth, String SelYear)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByDate_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(SelMonth)).Append(", ");
            sb.Append(sql.q(SelYear));
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetGeneralStatsByPublisher(int clientId, int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByPublisher ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetGeneralStatsByPublisher_AffSystem(int websiteId, int publisherId, int campaignId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByPublisher_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }


        public static DataTable Reports_GetGeneralStatsByWebsite(int clientId, int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByWebsite ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetGeneralStatsByWebsite_AffSystem(int websiteId, int publisherId, int campaignId, int lpOfferId, String SelYear, String SelMonth)
        {
            Debug.WriteLine("testing...before query");
            StringBuilder sb = new StringBuilder();
            Debug.WriteLine("SelYear:" + SelYear);
            Debug.WriteLine("SelMonth:" + SelMonth);
            sb.Append("EXEC Reports_GetGeneralStatsByWebsite_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", '");
            sb.Append(sql.q(SelYear)).Append("', '");
            sb.Append(sql.q(SelMonth)).Append("'");
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetGeneralStatsByWebsite_AffSystem_Temp(int websiteId, int publisherId, int campaignId, int lpOfferId, String SelYear, String SelMonth)
        {
            Debug.WriteLine("testing...before query");
            StringBuilder sb = new StringBuilder();
            Debug.WriteLine("SelYear:" + SelYear);
            Debug.WriteLine("SelMonth:" + SelMonth);
            sb.Append("EXEC Reports_GetGeneralStatsByWebsite_AffSystem_Temp ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", '");
            sb.Append(sql.q(SelYear)).Append("', '");
            sb.Append(sql.q(SelMonth)).Append("'");
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }


        public static DataTable Reports_GetGeneralStatsByCampaign(int clientId, int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByCampaign ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetGeneralStatsByCampaign_AffSystem(int websiteId, int publisherId, int campaignId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByCampaign_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetGeneralStatsByLandingPage(int clientId, int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetGeneralStatsByLandingPage ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetOfferStats(int clientId, int advertiserId, int offerId, int websiteId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetOfferStats ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(advertiserId)).Append(", ");
            sb.Append(sql.q(offerId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetOfferStatsByWebsite(int clientId, int advertiserId, int offerId, int websiteId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetOfferStatsByWebsite ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(advertiserId)).Append(", ");
            sb.Append(sql.q(offerId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetOfferStatsByCampaign(int clientId, int advertiserId, int offerId, int websiteId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetOfferStatsByCampaign ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(advertiserId)).Append(", ");
            sb.Append(sql.q(offerId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetOfferStatsByLandingPage(int clientId, int advertiserId, int offerId, int websiteId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetOfferStatsByLandingPage ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(advertiserId)).Append(", ");
            sb.Append(sql.q(offerId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }




        public static DataTable Reports_GetReferrerStatsByReferrer(int clientId, int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByReferrer ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrerStatsByDate(int clientId, int websiteId, int publisherId, int campaignId, string affId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByDate ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", '");
            sb.Append(sql.q(affId)).Append("', ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrerStatsByReferrer_AffSystem(int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByReferrer_AffSystem_Temp ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));
            Debug.WriteLine(sb);
            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrerStatsByDate_AffSystem(int websiteId, int publisherId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByDate_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }



        public static DataTable Reports_GetReferrerStatsByWebsite(int clientId, int websiteId, int publisherId, int referrerId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByWebsite ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(referrerId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrerStatsByWebsite_AffSystem(int websiteId, int publisherId, int referrerId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByWebsite_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(referrerId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrerStatsByCampaign(int clientId, int websiteId, int publisherId, int referrerId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByCampaign ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(referrerId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrerStatsByCampaign_AffSystem(int websiteId, int publisherId, int referrerId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByCampaign_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(referrerId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }


        public static DataTable Reports_GetReferrerStatsByLandingPage(int clientId, int websiteId, int publisherId, int referrerId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByLandingPage ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(referrerId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrerStatsByLandingPage_AffSystem(int websiteId, int publisherId, int referrerId, int campaignId, int lpOfferId, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrerStatsByLandingPage_AffSystem ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append(sql.q(referrerId)).Append(", ");
            sb.Append(sql.q(campaignId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataTable(sb.ToString());
        }


        public static DataSet Reports_GetSessionLeads(int clientId, int pageSize, int pageIndex,
            int websiteId, int lpOfferId, int publisherId,
            string emailAddress, string firstName, string lastName, byte matchMethod,
            DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetSessionLeads ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(pageSize)).Append(", ");
            sb.Append(sql.q(pageIndex)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append("'").Append(sql.q(emailAddress)).Append("', ");
            sb.Append("'").Append(sql.q(firstName)).Append("', ");
            sb.Append("'").Append(sql.q(lastName)).Append("', ");
            sb.Append(sql.q(matchMethod)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            return sql.GetDataSet(sb.ToString());
        }

        public static DataSet Reports_GetSessionLeads_AffSystem(int clientId, int pageSize, int pageIndex,
            int websiteId, int lpOfferId, int publisherId,
            string emailAddress, string firstName, string lastName, byte matchMethod,
            DateTime startDate, DateTime endDate, bool AllDates)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetSessionLeads_AffSystem ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(pageSize)).Append(", ");
            sb.Append(sql.q(pageIndex)).Append(", ");
            sb.Append(sql.q(websiteId)).Append(", ");
            sb.Append(sql.q(lpOfferId)).Append(", ");
            sb.Append(sql.q(publisherId)).Append(", ");
            sb.Append("'").Append(sql.q(emailAddress)).Append("', ");
            sb.Append("'").Append(sql.q(firstName)).Append("', ");
            sb.Append("'").Append(sql.q(lastName)).Append("', ");
            sb.Append(sql.q(matchMethod)).Append(", ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate)).Append(", ");
            sb.Append(sql.q(AllDates));
            Debug.WriteLine(sb);
            return sql.GetDataSet(sb.ToString());
        }


        public static DataTable Reports_GetLPOffers(int clientId, int campaignId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetLPOffers ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(campaignId));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetLPOffers_AffSystem(int PublisherID, int campaignId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetLPOffers_AffSystem ");
            sb.Append(sql.q(PublisherID)).Append(", ");
            sb.Append(sql.q(campaignId));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataTable Reports_GetReferrers(int PublisherID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetReferrers ");
            sb.Append(sql.q(PublisherID));

            return sql.GetDataTable(sb.ToString());
        }

        public static DataRow Reports_GetLeadDetails(int clientId, int leadId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetLeadDetails ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(leadId));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Reports_GetSessionLeadDetails(int clientId, int sessionId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetSessionLeadDetails ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(sessionId));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataRow Reports_GetOfferLeadDetails(int clientId, int leadId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetOfferLeadDetails ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(leadId));

            return sql.GetDataRow(sb.ToString());
        }

        public static DataTable Reports_GetSessionOfferImpressions(int clientId, int sessionId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_GetSessionOfferImpressions ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(sessionId));

            return sql.GetDataTable(sb.ToString());
        }

        public static bool Reports_UpdateOfferImpressionBillableStatus(int clientId, int offerImpressionId, bool billable)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_UpdateOfferImpressionBillableStatus ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(offerImpressionId)).Append(", ");
            sb.Append(sql.q(billable));

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Reports_UpdateOfferImpressionBillableByAdvertiser(int clientId, int advertiserId, int offerImpressionId, bool billable)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_UpdateOfferImpressionBillableByAdvertiser ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(advertiserId)).Append(", ");
            sb.Append(sql.q(offerImpressionId)).Append(", ");
            sb.Append(sql.q(billable));

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }

        public static bool Reports_UpdateSessionPayableStatus(int clientId, int sessionId, bool isPayable)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_UpdateSessionPayableStatus ");
            sb.Append(sql.q(clientId)).Append(", ");
            sb.Append(sql.q(sessionId)).Append(", ");
            sb.Append(sql.q(isPayable));

            return Convert.ToBoolean(sql.GetScalar(sb.ToString()));
        }


        public static void Reports_UpdateReportSummary_General_DateRange(DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_UpdateReportSummary_General_DateRange ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            sql.ExecNonQuery(sb.ToString());
        }

        public static void Reports_UpdateReportSummary_Offers_DateRange(DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Reports_UpdateReportSummary_Offers_DateRange ");
            sb.Append(sql.q(startDate)).Append(", ");
            sb.Append(sql.q(endDate));

            sql.ExecNonQuery(sb.ToString());
        }


        /* -- REPORTING FUNCTIONS [END] -- */


        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----
        // ----  ----  ----  ----  ----  ----  ----  ----


        /* -- MISCELLANEOUS FUNCTIONS [START] -- */

        public static DataTable GetMatchingKeywords(string Text)
        {
            string strSQL = "EXEC GetMatchingKeywords '" + sql.q(Text) + "'";
            return sql.GetDataTable(strSQL);
        }

        public static DataTable GetChildKeywords(int ParentKeywordID)
        {
            string strSQL = "EXEC GetChildKeywords " + sql.q(ParentKeywordID);
            return sql.GetDataTable(strSQL);
        }

        public static DataTable GetISO3166Countries()
        {
            return sql.GetDataTable("EXEC GetISO3166Countries");
        }

        public static DataTable GetISO3166Regions(string CountryCode)
        {
            string strSQL = "EXEC GetISO3166Regions '" + sql.q(CountryCode) + "'";
            return sql.GetDataTable(strSQL);
        }

        public static DataTable GetMatchingLocations(int MaxMatchCount, string Text)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC GetMatchingLocations ");
            sb.Append(sql.q(MaxMatchCount)).Append(", ");
            sb.Append("'").Append(sql.q(Text)).Append("'");

            return sql.GetDataTable(sb.ToString());
        }

        public static DataRow GetISO3166CodeDetails(string Code)
        {
            string strSQL = "EXEC GetISO3166CodeDetails '" + sql.q(Code) + "'";
            return sql.GetDataRow(strSQL);
        }

        public static DataRow GetStateName(string Abbreviation)
        {
            string strSQL = "EXEC GetStateName '" + sql.q(Abbreviation) + "'";
            return sql.GetDataRow(strSQL);
        }

        public static DataRow Flow_Getcitystatefromzipcode(string zipcode)
        {
            string strSQL = "EXEC Flow_Getcitystatefromzipcode '" + sql.q(zipcode) + "'";
            return sql.GetDataRow(strSQL);
        }

        private static string GetLeadValue(Hashtable Lead, string Field)
        {
            return FlowDirector.GetLeadValue(Lead, Field);
        }

        public static string GetLocationCodeByIPNumber(long IPNumber)
        {
            string strSQL = "EXEC GetLocationCodeByIPNumber " + sql.q(IPNumber);

            return sql.GetScalar(strSQL).ToString();
        }

        /* -- MISCELLANEOUS FUNCTIONS [END] -- */
        

        public static SqlDataReader AddiContactUnsub(string email, int campaignid)
        {
            
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC [InsertNewUnsub] ");
            sb.Append(sql.q(campaignid)).Append(", ");
            sb.Append("'").Append(sql.q(email)).Append("'");

            return sql.GetDataReader(sb.ToString());

        }
        public static DataTable GetEmailUnsubList(int campaignid)
        {
            
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC [GetEmailUnsubList] ");
            sb.Append(sql.q(campaignid));

            return sql.GetDataTable(sb.ToString());

        }

        public static DataRow Flow_GetSessionInfo_By_Email(int leadid)
        {
            DataRow dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EXEC Flow_GetSessionInfo_By_Email ");
                sb.Append(sql.q(leadid));

                dr = sql.GetDataRow(sb.ToString());
            }
            catch (Exception ex) { }

            return dr;
        }

        public static DataRow Partners_GetOne(int partnerid)
        {
            DataRow dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EXEC Partners_GetOne ");
                sb.Append(sql.q(partnerid));

                dr = sql.GetDataRow(sb.ToString());
            }
            catch (Exception ex) { }

            return dr;
        }

        public static DataTable Content_GetCategories(int websiteid)
        {
            DataTable dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EXEC Content_GetCategories ");
                sb.Append(sql.q(websiteid));

                dr = sql.GetDataTable(sb.ToString());
            }
            catch (Exception ex) { }

            return dr;
        }

        public static DataTable Content_GetContents(int categoryid)
        {
            DataTable dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EXEC Content_GetSections ");
                sb.Append(sql.q(categoryid));

                dr = sql.GetDataTable(sb.ToString());
            }
            catch (Exception ex) { }

            return dr;
        }

        public static SqlDataReader GetLeadData(int leadId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXEC Partners_GetLeadData2 ");
            sb.Append(sql.q(leadId));

            return sql.GetDataReader(sb.ToString());
        }
    }
    
}