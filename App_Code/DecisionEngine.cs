﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Diagnostics;

namespace NSS.ROS
{
    /// <summary>
    /// Decision engine.
    /// </summary>
    public static class DecisionEngine
    {

        public static bool CheckSeedingSystem(int CampaignID, int OfferIndex)
        {
            bool IsSeedingPosition = ros.Flow_CheckSeedingSystem(CampaignID, OfferIndex);
            return IsSeedingPosition;
        }

        public static DataRow DrawSeededOffer(int CampaignID, int OfferIndex)
        {
            DataRow returnRow = ros.Flow_DrawSeededOffer(CampaignID, OfferIndex);
            return returnRow;
        }

        public static DataRow DrawBestOffer(DataTable offers)
        {
            // [COMPLETE?]

            // if there's only one offer available, return it right away; avoid further costs
            if (offers.Rows.Count == 1)
                return offers.Rows[0];

            DataRow ret = null;

            // absolute scores: 0+
            Type _float = new float().GetType();
            offers.Columns.Add("AbsDemographicAgeScore", _float);
            offers.Columns.Add("AbsDemographicGenderScore", _float);
            offers.Columns.Add("AbsDemographicIncomeScore", _float);
            offers.Columns.Add("AbsTotalDemographicScore", _float);
            offers.Columns.Add("AbsGeographicScore", _float);
            offers.Columns.Add("AbsKeywordMatchScore", _float);
            offers.Columns.Add("AbsSequentialScore", _float);
            offers.Columns.Add("AbsBehaviouralScore", _float);
            offers.Columns.Add("AbsTotalScore", _float);

            // relative scores: 0-1 (float)
            offers.Columns.Add("RelDemographicAgeScore", _float);
            offers.Columns.Add("RelDemographicGenderScore", _float);
            offers.Columns.Add("RelDemographicIncomeScore", _float);
            offers.Columns.Add("RelGeographicScore", _float);
            offers.Columns.Add("RelKeywordMatchScore", _float);
            offers.Columns.Add("RelSequentialScore", _float);
            offers.Columns.Add("RelBehaviouralScore", _float);

            // retrieve cached relative scores
            FillCachedScores(offers);

            //// set relative behavioural scores
            //CompileBehaviouralScores(offers);

            // build all absolute scores
            CompileFinalScores(offers);

            // determine and return highest scoring row
            // [TESTED]
            float maxScore = -1;
            foreach (DataRow row in offers.Rows)
            {
                if (Convert.ToSingle(row["AbsTotalScore"]) > maxScore)
                {
                    maxScore = Convert.ToSingle(row["AbsTotalScore"]);
                    ret = row;
                }
            }

            return ret;
        }

        public static void CompileInitialScores() {
            // [TESTED]

            // calculates all of scores (including sequential) for the session & stores them
            // ->   this should be called as soon as the landing page is completed as it calls GetFilteredOffers,
            //      returning all remaining valid offers

            Type _float = new float().GetType();

            int intSessionID = FlowDirector.CurrentSessionID;
            DataTable dtOffers = ros.Flow_GetFilteredOffers(intSessionID);

            dtOffers.Columns.Add("RelDemographicAgeScore",  _float);
            dtOffers.Columns.Add("RelDemographicGenderScore", _float);
            dtOffers.Columns.Add("RelDemographicIncomeScore", _float);
            dtOffers.Columns.Add("RelGeographicScore", _float);
            dtOffers.Columns.Add("RelKeywordMatchScore", _float);
            dtOffers.Columns.Add("RelSequentialScore", _float);
	    dtOffers.Columns.Add("RelBehaviouralScore", _float);


            // reuse connection
            using (SqlConnection cn = sql.GetConnection())
            {
                cn.Open();

                // calculate scores
                CalculateDemographicScores(cn, dtOffers);

                CalculateGeographicScores(cn, dtOffers);
                CalculateKeywordMatchScores(cn, dtOffers);
                CalculateSequentialScores(cn, dtOffers);
		CompileBehaviouralScores(cn, dtOffers);

                // store
                foreach (DataRow row in dtOffers.Rows)
                {
                    ros.Analytics_CacheOfferScores(
                        cn, intSessionID, Convert.ToInt32(row["OfferID"]),
                        Convert.ToSingle(row["RelDemographicAgeScore"]),
                        Convert.ToSingle(row["RelDemographicGenderScore"]),
                        Convert.ToSingle(row["RelDemographicIncomeScore"]),
                        Convert.ToSingle(row["RelGeographicScore"]),
                        Convert.ToSingle(row["RelKeywordMatchScore"]),
                        Convert.ToSingle(row["RelSequentialScore"]),
			Convert.ToSingle(row["RelBehaviouralScore"])
			);
                }

                // CompileFinalScores doesn't really have to be called here -- although it
                // may be best to store AbsTotalScore here and retrieve it later instead of
                // retrieving the relative scores later on
                // -> compile & save total scores
                dtOffers.Columns.Add("AbsDemographicAgeScore", _float);
                dtOffers.Columns.Add("AbsDemographicGenderScore", _float);
                dtOffers.Columns.Add("AbsDemographicIncomeScore", _float);
                dtOffers.Columns.Add("AbsTotalDemographicScore", _float);
                dtOffers.Columns.Add("AbsGeographicScore", _float);
                dtOffers.Columns.Add("AbsKeywordMatchScore", _float);
                dtOffers.Columns.Add("AbsSequentialScore", _float);
                dtOffers.Columns.Add("AbsBehaviouralScore", _float);
		dtOffers.Columns.Add("AbsTotalScore", _float);
		

                CompileFinalScores(dtOffers);
                foreach (DataRow row in dtOffers.Rows)
                {
                    ros.Analytics_StoreTotalScore(cn,
                        intSessionID, Convert.ToInt32(row["OfferID"]),
                        Convert.ToSingle(row["AbsTotalScore"]));
                }
            }

        }

        private static void CompileFinalScores(DataTable Offers)
        {
            // [TESTED]
            int intTotalDemoWeight = GetWeight("DW_Demographic_Age") + GetWeight("DW_Demographic_Gender") + GetWeight("DW_Demographic_Income");
            foreach (DataRow row in Offers.Rows)
            {
                // calculate demographic scores
                float fTotalDemoScore = 0;

                float fAgeScore = Convert.ToSingle(row["RelDemographicAgeScore"]);       // 0-1f
                float fGenderScore = Convert.ToSingle(row["RelDemographicGenderScore"]); // 0-1f
                float fIncomeScore = Convert.ToSingle(row["RelDemographicIncomeScore"]); // 0-1f

                fTotalDemoScore =
                    (fAgeScore * (float)GetWeight("DW_Demographic_Age") +
                    fGenderScore * (float)GetWeight("DW_Demographic_Gender") +
                    fIncomeScore * (float)GetWeight("DW_Demographic_Income"))
                    / (float)intTotalDemoWeight;

                // temp/set for testing
                row["AbsDemographicAgeScore"] = fAgeScore * (float)GetWeight("DW_Demographic_Age");
                row["AbsDemographicGenderScore"] = fGenderScore * (float)GetWeight("DW_Demographic_Gender");
                row["AbsDemographicIncomeScore"] = fIncomeScore * (float)GetWeight("DW_Demographic_Income");

                // total demographic
                row["AbsTotalDemographicScore"] = fTotalDemoScore * (float)GetWeight("DW_Demographic");

                // calculate other scores
                row["AbsGeographicScore"] = Convert.ToSingle(row["RelGeographicScore"]) * (float)GetWeight("DW_Geographic");
                row["AbsKeywordMatchScore"] = Convert.ToSingle(row["RelKeywordMatchScore"]) * (float)GetWeight("DW_Keywords");
                row["AbsBehaviouralScore"] = Convert.ToSingle(row["RelBehaviouralScore"]) * (float)GetWeight("DW_Behavioural");
                row["AbsSequentialScore"] = Convert.ToSingle(row["RelSequentialScore"]) * (float)GetWeight("DW_Sequential");

                // total absolutes
                row["AbsTotalScore"] =
                    Convert.ToInt32(row["AbsTotalDemographicScore"])
                    + Convert.ToInt32(row["AbsGeographicScore"])
                    + Convert.ToInt32(row["AbsKeywordMatchScore"])
                    + Convert.ToInt32(row["AbsSequentialScore"])
                    + Convert.ToInt32(row["AbsBehaviouralScore"]);
            }

            //// temp/testing
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //foreach (DataRow row in Offers.Rows)
            //{
            //    sb.Append(row["OfferID"].ToString()).Append(",");
            //    sb.Append((int)Context.Session["PreviousOfferID"]).Append(",---,");
            //    sb.Append(row["RelDemographicAgeScore"].ToString()).Append(",");
            //    sb.Append(row["RelDemographicGenderScore"].ToString()).Append(",");
            //    sb.Append(row["RelDemographicIncomeScore"].ToString()).Append(",-,");
            //    sb.Append(row["RelGeographicScore"].ToString()).Append(",");
            //    sb.Append(row["RelKeywordMatchScore"].ToString()).Append(",");
            //    //sb.Append(row["RelBehaviouralScore"].ToString()).Append(",---,");
            //    sb.Append(row["RelSequentialScore"].ToString()).Append(",---,");
            //    sb.Append(row["AbsDemographicAgeScore"].ToString()).Append(",");
            //    sb.Append(row["AbsDemographicGenderScore"].ToString()).Append(",");
            //    sb.Append(row["AbsDemographicIncomeScore"].ToString()).Append(",[");
            //    sb.Append(row["AbsTotalDemographicScore"].ToString()).Append("],");
            //    sb.Append(row["AbsGeographicScore"].ToString()).Append(",");
            //    sb.Append(row["AbsKeywordMatchScore"].ToString()).Append(",");
            //    //sb.Append(row["AbsBehaviouralScore"].ToString()).Append(",-,");
            //    sb.Append(row["AbsSequentialScore"].ToString()).Append(",-,");
            //    sb.Append(row["AbsTotalScore"].ToString());
            //    sb.Append("\r\n");
            //}
            //sb.Append("\r\n");

            ////System.IO.File.AppendAllText("e:\\de.txt", sb.ToString());
        }

        private static void FillCachedScores(DataTable offers)
        {
            // [TESTED]
            DataTable dtCached = ros.Analytics_GetCachedScores(FlowDirector.CurrentSessionID);
            foreach (DataRow rowOffer in offers.Rows)
            {
                DataRow[] foo = dtCached.Select("OfferID = " + rowOffer["OfferID"].ToString());
                if (foo.Length > 0) {
                    DataRow rowCached = foo[0];
                    rowOffer["RelDemographicAgeScore"] = rowCached["DemographicAgeScore"];
                    rowOffer["RelDemographicGenderScore"] = rowCached["DemographicGenderScore"];
                    rowOffer["RelDemographicIncomeScore"] = rowCached["DemographicIncomeScore"];
                    rowOffer["RelGeographicScore"] = rowCached["GeographicScore"];
                    rowOffer["RelKeywordMatchScore"] = rowCached["KeywordMatchScore"];
                    rowOffer["RelSequentialScore"] = rowCached["SequentialScore"];
                    rowOffer["RelBehaviouralScore"] = rowCached["BehaviouralScore"];
                }
            }
        }

        private static int GetWeight(string Key)
        {
            // [TESTED]
            return int.Parse(System.Configuration.ConfigurationManager.AppSettings[Key]);
        }

        private static void CalculateDemographicScores(SqlConnection cn, DataTable offers)
        {
            // [TESTED]
            Hashtable lead = FlowDirector.CurrentLead;
            //int intAge = ros.Analytics_GetNormalizedAge(Utility.GetDerivedAge(FlowDirector.GetLeadValue(hshLead, "DateOfBirth")));
            //string strGender = Utility.GetNormalizedGender(FlowDirector.GetLeadValue(hshLead, "Gender"));
            //int intIncome = Utility.GetNormalizedIncome(FlowDirector.GetLeadValue(hshLead, "Income"));
            short age = (short)lead["_NormalAge"];
            string gender = (string)lead["_NormalGender"];
            int income = (int)lead["_NormalIncome"];

            // this will set DemographicAgeScore, DemographicGenderScore & DemographicIncomeScore
            foreach (DataRow row in offers.Rows)
            {
                DataRow rowScores = ros.Analytics_GetDemographicScores(
                    cn, Convert.ToInt32(row["OfferID"]), age, gender, income);

                row["RelDemographicAgeScore"] = rowScores["AgeScore"];
                row["RelDemographicGenderScore"] = rowScores["GenderScore"];
                row["RelDemographicIncomeScore"] = rowScores["IncomeScore"];
            }
        }

        private static void CalculateGeographicScores(SqlConnection cn, DataTable offers)
        {
            // [TESTED]
            string locationCode = Sessions.Visitor["LocationCode"].ToString();

            foreach (DataRow row in offers.Rows)
            {
                DataRow rowScore = ros.Analytics_GetGeographicScore(cn,
                    Convert.ToInt32(row["OfferID"]), locationCode);

                row["RelGeographicScore"] = rowScore[0];
            }
        }

        private static void CalculateKeywordMatchScores(SqlConnection cn, DataTable Offers)
        {
            // [TESTED]
 
            // score calculation:
            // -> a = total referrer keyword count
            // -> b = total offer keyword count
            // -> c = number of offer keywords matching the referrer keywords
            // -> d = number of referrer keywords matching the offer keywords
            // -> score = (c + d) / (a + b)
            // -> note: c & d are given full scores (1) for each exact match; 
            //          for partial matches (parent-child and/or sibling), scores are 0.5 (parent-child) plus/or 0.25 (sibling)

            const float __PARENT_CHILD = 0.5f;
            const float __SIBLING = 0.25f;

            DataTable dtReferrerKeywords = ros.Analytics_GetReferrerKeywords(cn, FlowDirector.CurrentSessionID);

            if (dtReferrerKeywords.Rows.Count == 0)
            {
                // no referrer keywords, so the score for all offers is 0;
                foreach (DataRow row in Offers.Rows)
                    row["RelKeywordMatchScore"] = 0f;
            }
            else
            {
                foreach (DataRow row in Offers.Rows)
                {
                    DataTable dtOfferKeywords = ros.Analytics_GetOfferKeywords(cn, Convert.ToInt32(row["OfferID"]));

                    if (dtReferrerKeywords.Rows.Count == 0 || dtOfferKeywords.Rows.Count == 0)
                        row["RelKeywordMatchScore"] = 0f;
                    else
                    {
                        float fNum = 0; // numerator
                        int intDenom = dtReferrerKeywords.Rows.Count + dtOfferKeywords.Rows.Count;  // denominator

                        // match referrer keywords to offer
                        foreach (DataRow rowRefKey in dtReferrerKeywords.Rows)
                        {
                            bool bExactMatch = false;
                            bool bParentChildMatch = false;
                            bool bSiblingMatch = false;

                            foreach (DataRow rowOfferKey in dtOfferKeywords.Rows)
                            {
                                int offerKeywordID = Convert.ToInt32(rowOfferKey["KeywordID"]);
                                int refKeywordID = Convert.ToInt32(rowRefKey["KeywordID"]);
                                int offerParentKeywordID = rowOfferKey["ParentKeywordID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(rowOfferKey["ParentKeywordID"]);
                                int refParentKeywordID = rowRefKey["ParentKeywordID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(rowRefKey["ParentKeywordID"]);

                                if (offerKeywordID == refKeywordID)
                                {
                                    bExactMatch = true;
                                    break;  // max score reached; break
                                }
                                // no sibling match if the parent keywords are null (if both are at the root)
                                else if (offerParentKeywordID == refParentKeywordID && offerParentKeywordID != int.MinValue)
                                {
                                    bSiblingMatch = true;
                                }
                                else if (offerParentKeywordID == refKeywordID || offerKeywordID == refParentKeywordID)
                                {
                                    bParentChildMatch = true;
                                }
                            }

                            if (bExactMatch)
                                fNum += 1;
                            else
                            {
                                if (bParentChildMatch)
                                    fNum += __PARENT_CHILD;

                                if (bSiblingMatch)
                                    fNum += __SIBLING;
                            }
                        }

                        // match offer keywords to referrer
                        foreach (DataRow rowOfferKey in dtOfferKeywords.Rows)
                        {
                            bool bExactMatch = false;
                            bool bParentChildMatch = false;
                            bool bSiblingMatch = false;

                            foreach (DataRow rowRefKey in dtReferrerKeywords.Rows)
                            {
                                int offerKeywordID = Convert.ToInt32(rowOfferKey["KeywordID"]);
                                int refKeywordID = Convert.ToInt32(rowRefKey["KeywordID"]);
                                int offerParentKeywordID = rowOfferKey["ParentKeywordID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(rowOfferKey["ParentKeywordID"]);
                                int refParentKeywordID = rowRefKey["ParentKeywordID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(rowRefKey["ParentKeywordID"]);

                                if (offerKeywordID == refKeywordID)
                                {
                                    bExactMatch = true;
                                    break;  // max score reached; break
                                }
                                else if (offerParentKeywordID == refParentKeywordID && offerParentKeywordID != int.MinValue)
                                {
                                    bSiblingMatch = true;
                                }
                                else if (offerParentKeywordID == refKeywordID || offerKeywordID == refParentKeywordID)
                                {
                                    bParentChildMatch = true;
                                }
                            }

                            if (bExactMatch)
                                fNum += 1;
                            else
                            {
                                if (bParentChildMatch)
                                    fNum += __PARENT_CHILD;

                                if (bSiblingMatch)
                                    fNum += __SIBLING;
                            }
                        }

                        row["RelKeywordMatchScore"] = fNum / (float)intDenom;
                    }
                }
            }
        }

        //private static void CalculateKeywordMatchScores_Obsolete(SqlConnection cn, DataTable Offers)
        //{
        //    // [TESTED]
        //    // todo/note: this works, but the scoring algorithm may have to be changed if something better
        //    // can be developed

        //    // score calculation:
        //    // -> a = total referrer keyword count
        //    // -> b = total offer keyword count
        //    // -> c = number of offer keywords matching the referrer keywords
        //    // -> d = number of referrer keywords matching the offer keywords
        //    // -> score = (c + d) / (a + b)

        //    DataTable dtReferrerKeywords = ros.Analytics_GetReferrerKeywords(cn, FlowDirector.CurrentSessionID);

        //    if (dtReferrerKeywords.Rows.Count == 0)
        //    {
        //        // no referrer keywords, so the score for all offers is 0;
        //        foreach (DataRow row in Offers.Rows)
        //            row["RelKeywordMatchScore"] = 0f;
        //    }
        //    else
        //    {
        //        foreach (DataRow row in Offers.Rows)
        //        {
        //            DataTable dtOfferKeywords = ros.Analytics_GetOfferKeywords(cn, Convert.ToInt32(row["OfferID"]));

        //            if (dtReferrerKeywords.Rows.Count == 0 || dtOfferKeywords.Rows.Count == 0)
        //                row["RelKeywordMatchScore"] = 0f;
        //            else
        //            {
        //                int intNum = 0;
        //                int intDenom = dtReferrerKeywords.Rows.Count + dtOfferKeywords.Rows.Count;

        //                // match referrer keywords to offer
        //                foreach (DataRow rowRefKey in dtReferrerKeywords.Rows)
        //                {
        //                    foreach (DataRow rowOfferKey in dtOfferKeywords.Rows)
        //                    {
        //                        if (Convert.ToInt32(rowOfferKey["KeywordID"]) == Convert.ToInt32(rowRefKey["KeywordID"]))
        //                        {
        //                            intNum++;
        //                            break;
        //                        }
        //                    }
        //                }

        //                // match offer keywords to referrer
        //                foreach (DataRow rowOfferKey in dtOfferKeywords.Rows)
        //                {
        //                    foreach (DataRow rowRefKey in dtReferrerKeywords.Rows)
        //                    {
        //                        if (Convert.ToInt32(rowOfferKey["KeywordID"]) == Convert.ToInt32(rowRefKey["KeywordID"]))
        //                        {
        //                            intNum++;
        //                            break;
        //                        }
        //                    }
        //                }

        //                row["RelKeywordMatchScore"] = (float)intNum / (float)intDenom;
        //            }
        //        }
        //    }
        //}

        private static void CompileBehaviouralScores(SqlConnection cn, DataTable Offers)
        {
            // [TESTED]
            // todo: update based on new calculation algorithm

            int intPreviousOfferID = (int)Sessions.Visitor["PreviousOfferID"];

                //cn.Open();
                // set BehaviouralScore
                foreach (DataRow row in Offers.Rows)
                {
                    DataRow rowScore = ros.Analytics_GetBehaviouralScore(cn,
                        Convert.ToInt32(row["OfferID"]), intPreviousOfferID);

                    row["RelBehaviouralScore"] = rowScore[0];
                }
     
        }

        private static void CalculateSequentialScores(SqlConnection cn, DataTable Offers)
        {
            // [TESTED]
            List<int> intOffers = CalculateBestPath(cn, Offers);
            int intPreviousOfferID = (int)Sessions.Visitor["PreviousOfferID"];
            int intOfferCount = intOffers.Count;

            float fShare;
            if (intOffers.Count == 0)
                return;
            else if(intOffers.Count == 1)
                fShare = 0f;
            else
                fShare = (float)Math.Round((decimal) (1f / (float)(intOffers.Count - 1)), 3);

            // set SequentialScore
            for (int i = 0; i < intOffers.Count; i++)
            {
                DataRow row = Offers.Select("OfferID = " + intOffers[i])[0];
                float fScore = (1 - i * fShare);
                if (fScore < 0)
                    fScore = 0;
                row["RelSequentialScore"] = fScore;
            }
        }

        private static List<int> CalculateBestPath(SqlConnection cn, DataTable Offers)
        {
            // [TESTED]
            // - returns an ordered list of offers

            DataTable dtOffers = Offers.Copy(); // cloned

            List<int> ret = new List<int>();  // list: OfferID

            // get & add first offer (following the landing page)
            int intNextOfferID = GetBestSubsequentOffer(cn, (int)Sessions.Visitor["LandingPageOfferID"], dtOffers);
            ret.Add(intNextOfferID);

            // remove next offer 
            int intOfferID = intNextOfferID;
            dtOffers.Rows.Remove(dtOffers.Select("OfferID = " + intOfferID.ToString())[0]);

            while (dtOffers.Rows.Count > 0)
            {
                intNextOfferID = GetBestSubsequentOffer(cn, intOfferID, dtOffers);              // get
                ret.Add(intNextOfferID);                                                        // add
                intOfferID = intNextOfferID;                                                    // update
                dtOffers.Rows.Remove(dtOffers.Select("OfferID = " + intOfferID.ToString())[0]); // remove
            }

            return ret;
        }

        private static int GetBestSubsequentOffer(SqlConnection cn, int CurrentOfferID, DataTable Offers)
        {
            // [TESTED]
            // returns the subsequent offer with the highest score (conversion rate)
            int ret = int.MinValue;

            float fBestScore = float.MinValue;
            foreach (DataRow row in Offers.Rows) {
                int intNextOfferID = Convert.ToInt32(row["OfferID"]);
                if (intNextOfferID != CurrentOfferID)
                {
                    float fScore = ros.Analytics_GetSequentialScore(cn, CurrentOfferID, intNextOfferID);
                    if (fScore > fBestScore)
                    {
                        fBestScore = fScore;
                        ret = intNextOfferID;
                    }
                }
            }

            return ret;
        }

        public static DataRow GetRandomOffer(DataTable Offers)
        {
            // [TESTED]

            DataTable dtClone = Offers.Copy();
            int impressionThreshold = int.Parse(Utility.GetSetting("RandomOfferMaxImpressionCt"));
            Random r = new Random(DateTime.Now.Millisecond);

            // select random offer
            while (dtClone.Rows.Count > 0)
            {
                DataRow row = dtClone.Rows[r.Next(dtClone.Rows.Count)];
                int intOfferID = Convert.ToInt32(row["OfferID"]);
                if (ros.Flow_GetOfferImpressionCount(intOfferID) < impressionThreshold)
                    return row;
                else
                    dtClone.Rows.Remove(row);
            }

            return null;
        }
    
    }
}
