﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace NSS.ROS
{
    /// <summary>
    /// Weekdays at which a live feed runs when its schedule type is set to Daily.
    /// Bits 1-7 represent Sunday-Saturday respectively.
    /// </summary>
    [Flags]
    public enum FeedWeekdays
    {
        Sunday = 0x01,
        Monday = 0x02,
        Tuesday = 0x04,
        Wednesday = 0x08,
        Thursday = 0x10,
        Friday = 0x20,
        Saturday = 0x40,
        Everyday = 0x7F
    }

    public enum FeedFileFormat
    {
        Text = 1//,
        //Zip = 2   // not implemented
    }

    public enum FeedDeliveryMethod
    {
        HttpGet = 1,
        HttpPost = 2,
        HttpRPC = 3,
        Email = 4,
        FTP = 5
    }

    public enum FeedScheduleType
    {
        Interval = 1,
        Daily = 2
    }

    public enum FeedTaskStatus
    {
        Ready = 1,
        Complete = 2,
        Failed = 3
    }

    public enum FeedLeadStatus
    {
        Ready = 1,
        Accepted = 2,
        Rejected = 3,
        Failed = 4
    }

    /// <summary>
    /// Live feed.
    /// </summary>
    public static class Feed
    {
        public static void Execute()
        {
            SqlConnection cn = sql.GetConnection();
            cn.Open();

            DataTable dtTasks = ros.Feed_GetPendingTasks(cn);
            foreach (DataRow rowTask in dtTasks.Rows)
            {
                int intFeedID = Convert.ToInt32(rowTask["FeedID"]);
                int intRunID = ros.Feed_CreateRun(cn, intFeedID);
                FeedDeliveryMethod enuDeliveryMethod = (FeedDeliveryMethod)Convert.ToByte(rowTask["DeliveryMethod"]);
                string strDeliveryURL = rowTask["DeliveryURL"].ToString();
                string strDeliveryTemplate = rowTask["DeliveryTemplate"].ToString();

                ros.Feed_UpdateFeedLastRunTime(cn, intFeedID);
                ros.Feed_MoveQueuedLeadsToRun(cn, intFeedID, intRunID);
                DataTable dtLeads = ros.Feed_GetRunLeads(cn, intRunID);

                bool bSuccess = false;
                string strErrorMessage = "";

                if (dtLeads.Rows.Count == 0)
                {
                    // no leads to feed; next
                    bSuccess = true;
                }
                else
                {
                    // todo: cleanup

                    switch (enuDeliveryMethod)
                    {
                        case FeedDeliveryMethod.Email:
                            {
                                string strCompiled = GetCompiledLeads(strDeliveryTemplate, dtLeads);
                                string strFeedFilePath = SaveFeedFile(DecodeFileName(rowTask["FileName"].ToString()), strCompiled);
                                bSuccess = EmailFile(
                                    rowTask["EmailFromAddress"].ToString(), rowTask["EmailToAddress"].ToString(),
                                    rowTask["EmailSubject"].ToString(), strFeedFilePath, out strErrorMessage);
                                break;
                            }

                        case FeedDeliveryMethod.FTP:
                            {
                                string strCompiled = GetCompiledLeads(strDeliveryTemplate, dtLeads);
                                string strFeedFilePath = SaveFeedFile(DecodeFileName(rowTask["FileName"].ToString()), strCompiled);
                                bSuccess = TransferFile(
                                    rowTask["FTPHost"].ToString(), Convert.ToUInt16(rowTask["FTPPort"]),
                                    rowTask["FTPUsername"].ToString(), rowTask["FTPPassword"].ToString(),
                                    rowTask["FTPTargetPath"].ToString(), strFeedFilePath, out strErrorMessage);
                                break;
                            }

                        case FeedDeliveryMethod.HttpGet:
                        case FeedDeliveryMethod.HttpPost:
                        case FeedDeliveryMethod.HttpRPC:
                            {
                                foreach (DataRow rowLead in dtLeads.Rows)
                                    ProcessHttpLead(rowTask, rowLead);
                                bSuccess = true;    // always set to true for Http requests
                                break;
                            }
                    }
                }

                ros.Feed_CompleteRun(cn, intRunID,
                    bSuccess ? FeedTaskStatus.Complete : FeedTaskStatus.Failed,
                    strErrorMessage);
            }
            cn.Close();
            cn.Dispose();
        }

        public static void EngageFeeds(int OfferImpressionID)
        {
            // NO FEEDS, DISABLE TO REDUCE LOAD ON Leads TABLE 2015-02-09
            return;

            if (OfferImpressionID == 0)
                return;

            SqlConnection cn = null;
            DataRow row = null;

            try
            {
                cn = sql.GetConnection();
                cn.Open();
                row = ros.Feed_GetOfferImpressionDetails(cn, OfferImpressionID);

                if (row == null)
                    throw new Exception("No row were returned");
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("Feed_GetOfferImpressionDetails", ex, "OfferImpressionID: " + OfferImpressionID.ToString());
                row = null;
            }
            finally
            {
                if (cn != null) { 
                    cn.Close();
                    cn.Dispose();
                }
            }

            if (row != null && row["LeadID"] != DBNull.Value)
            {
                int intOfferID = Convert.ToInt32(row["OfferID"]);
                int intLeadID = Convert.ToInt32(row["LeadID"]);

                DataTable dt = ros.Feed_GetApplicableFeeds(cn, intOfferID);
                foreach (DataRow rowFeed in dt.Rows)
                    ros.Feed_QueueLead(cn, Convert.ToInt32(rowFeed["FeedID"]), intLeadID);
            }
        }

        private static string DecodeFileName(string FileName)
        {
            // [INCOMPLETE]
            // todo: use Utility.DecodeString()?
            // -> another function required for dates
            string ret = FileName;
            DateTime dat = DateTime.Now;

            MatchCollection c = Regex.Matches(FileName, "\\[\\[#[\\w]*\\]\\]");
            foreach (Match m in c)
                ret = ret.Replace(m.Value, dat.ToString(m.Value.Substring(3, m.Value.Length - 5)));

            return ret;
        }

        private static string GetCompiledLeads(string DeliveryTemplate, DataTable Leads)
        {
            // [INCOMPLETE]
            // todo
            StringBuilder sb = new StringBuilder();
            foreach (DataRow row in Leads.Rows)
            {
                //HttpContext c = new HttpContext(
                sb.Append(DecodeString(row, DeliveryTemplate)).Append("\r\n");
            }

            return sb.ToString();
        }

        private static string SaveFeedFile(string FileName, string CompiledData)
        {
            // [UNTESTED]
            // returns file path
            string strPath = ConfigurationManager.AppSettings["FeedFilePath"] + FileName;
            File.WriteAllText(strPath, CompiledData);
            return strPath;
        }

        private static bool TransferFile(string Hostname, ushort Port, string Username, string Password, string TargetPath, string SourceFilePath, out string ErrorMessage)
        {
            // [TESTED]
            try
            {
                FileInfo fi = new FileInfo(SourceFilePath);
                FtpWebRequest req = (FtpWebRequest) FtpWebRequest.Create("ftp://" + Hostname + ":" + Port.ToString() + TargetPath + fi.Name);
                req.Method = WebRequestMethods.Ftp.UploadFile;
                req.Credentials = new NetworkCredential(Username, Password);
                req.UsePassive = true;
                req.UseBinary = true;
                req.KeepAlive = false;

                FileStream fs = File.OpenRead(SourceFilePath);
                byte[] buf = new byte[fs.Length];
                fs.Read(buf, 0, buf.Length);
                fs.Close();

                Stream reqStream = req.GetRequestStream();
                reqStream.Write(buf, 0, buf.Length);

                ErrorMessage = "";
                return true;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
        }

        private static bool EmailFile(string From, string To, string Subject, string SourceFilePath, out string ErrorMessage)
        {
            // [TESTED]
            try
            {
                SmtpClient c = new SmtpClient(
                    ConfigurationManager.AppSettings["Feed_SmtpHost"], ushort.Parse(ConfigurationManager.AppSettings["Feed_SmtpPort"]));

                MailMessage msg = new MailMessage(From, To, Subject, "");
                Attachment att = new Attachment(SourceFilePath);
                msg.Attachments.Add(att);
                c.Send(msg);

                ErrorMessage = "";
                return true;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
        }

        private static void ProcessHttpLead(DataRow Feed, DataRow Lead)
        {
            // [INCOMPLETE?] --> RPC (?)

            string strData = DecodeString(Lead, Feed["DeliveryTemplate"].ToString());
            int intFeedRunLeadID = Convert.ToInt32(Lead["FeedRunLeadID"]);
            string strResponse = "", strSuccessToken = "";
            FeedLeadStatus enuStatus = FeedLeadStatus.Ready;
            WebClient c = new WebClient();

            ros.Feed_UpdateRunLead(Convert.ToInt32(Lead["FeedRunLeadID"]), enuStatus, strData);

            try
            {
                switch ((FeedDeliveryMethod)Convert.ToInt32(Feed["DeliveryMethod"]))
                {
                    case FeedDeliveryMethod.HttpGet:
                        {
                            strResponse = c.DownloadString(Feed["DeliveryURL"].ToString() + "?" + strData);
                            break;
                        }
                    case FeedDeliveryMethod.HttpPost:
                        {
                            strResponse = c.UploadString(Feed["DeliveryURL"].ToString(), "POST", strData);
                            break;
                        }
                    case FeedDeliveryMethod.HttpRPC:
                        {
                            // any different from Post?
                            strResponse = c.UploadString(Feed["DeliveryURL"].ToString(), "POST", strData);
                            break;
                        }
                }

                Match m = Regex.Match(strResponse, Feed["SuccessTokenRegex"].ToString());
                strSuccessToken = m.Value;
                enuStatus = m.Success ? FeedLeadStatus.Accepted : FeedLeadStatus.Rejected;

            }
            catch (Exception ex)
            {
                enuStatus = FeedLeadStatus.Failed;
            }

            ros.Feed_CompleteRunLead(Convert.ToInt32(Lead["FeedRunLeadID"]), enuStatus, strResponse, strSuccessToken);
        }


        private static string DecodeString(DataRow Lead, string Value)
        {
            string ret = Value;
            string strTokenName;

            MatchCollection c = Regex.Matches(Value, "\\[\\[[^\\]]*\\]\\]");
            foreach (Match m in c)
            {
                strTokenName = m.Value.Substring(2, m.Value.Length - 4).ToUpper();
                ret = ret.Replace(m.Value, Utility.GetArrayValue(Lead, strTokenName));
            }
            return ret;
        }


    }
}