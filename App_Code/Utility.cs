﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.Web.SessionState;
using System.Web.UI.WebControls;
using System.Web.UI;
using Babel.Licensing;
namespace NSS.ROS
{
    /// <summary>
    /// Miscellaneous utility functions.
    /// </summary>
    public class Utility
    {
        public static string GetDefaultValue(System.Collections.Specialized.NameValueCollection Collection, string Name, string Default)
        {
            // [TESTED]
            return Collection[Name] == null ? Default : Collection[Name];
        }

        public static bool ValidateFolderAccess(int ClientID, string Path)
        {
            // [INCOMPLETE]
            return Path.IndexOf("..") == -1;
        }

        public static bool ValidateFileAccess(int ClientID, string Path)
        {
            // todo
            return true;
        }

        public static bool ValidateFolderName(string Name)
        {
            return Regex.IsMatch(Name, "^[\\w\\d\\-]+$");
        }

        public static string MapPath(int ClientID, string Path)
        {
            string ret = ConfigurationManager.AppSettings["HostingPath"] + "clientfiles\\" + ClientID.ToString()
                + Path.Replace("/", "\\");

            if (ret[ret.Length - 1] == '/')
                ret = ret.Substring(0, ret.Length - 1);

            return ret;
        }

        public static long GetIPNumber(string IPAddress)
        {
            //return System.Net.IPAddress.Parse(IPAddress).Address;

#if DEBUG

            return (long)127001;

#else
            // adapted from: http://geekswithblogs.net/rgupta/archive/2009/04/29/convert-ip-to-long-and-vice-versa-c.aspx
            string[] ipBytes;
            double num = 0;
            if (!string.IsNullOrEmpty(IPAddress))
            {
                ipBytes = IPAddress.Split('.');
                for (int i = ipBytes.Length - 1; i >= 0; i--)
                {
                    num += ((int.Parse(ipBytes[i]) % 256) * Math.Pow(256, (3 - i)));
                }
            }
            return (long)num;
#endif
        }

        public static short GetDerivedAge(object DateOfBirth)
        {
            if (DateOfBirth == null)
                return short.MinValue;

            short ret;

            DateTime dt;
            if (DateTime.TryParse(DateOfBirth.ToString(), out dt))
            {
                DateTime dtNow = DateTime.Now;
                ret = (short)(dtNow.Year - dt.Year);
                if (dtNow.Month < dt.Month || (dtNow.Month == dt.Month && dtNow.Day < dt.Day))
                    ret--;
            }
            else
            {
                ret = short.MinValue; // unknown
            }

            return ret;
        }

        public static string CompileURL(Hashtable Lead, string URL)
        {
            // todo: move to FlowDirector?
            string ret = URL;
            MatchCollection col = Regex.Matches(URL, "(?i)\\[\\[[^\\]]*\\]\\]");
            foreach (Match m in col)
                ret = ret.Replace(m.Value, FlowDirector.GetLeadValue(Lead, m.Value.Substring(2, m.Value.Length - 4)));

            return ret;
        }

        public static string DecodeRoot(int ClientID, string Path)
        {
            // decodes the root path only
            return Path.Replace("[[%FILEROOT]]",
                ConfigurationManager.AppSettings["HTMLHostingPath"] + "/clientfiles/" + ClientID.ToString());
        }

        //public static string DecodeString(HttpContext Context, string Value)
        //{
        //    // todo: second DecodeString constructor (with Session and Request objects) (?)
        //    //  -> to be used outside the flow; should be able to mimic a session state
        //}

        public static string DecodeString(HttpContext Context, string Value)
        {
            // [TESTED: OK]

            // todo: 
            // -> move to FlowDirector?
            // -> add [[#...]] (for dates: [[#yyyy-MM-dd]]) (?)
            // -> or: add functions (?):
            //  -> ^NOW         -> ex: [[^NOW]] == DateTime.Now (2009-10-27)
            //  -> ^DATE        -> ex: [[^DATE:^NOW,yyyy-MM-dd]] == 2009-10-27

            // [[xxx]]                  -> lead values
            // [[^FN:ARG1,ARG2,...]]    -> function(args); EX: UC (toUpper()), LC (toLower())
            // [[$xxx]]                 -> session values (SESSIONID [GUID], IPADDRESS, REFERRER, LOCATIONCODE)
            // [[%xxx]]                 -> client values (FILEROOT)

            Hashtable session = Sessions.Visitor;
            Hashtable Lead = FlowDirector.CurrentLead;

            // todo: should probably not kick out -- should just return nothing when data is missing
            if (Lead == null || session["SessionID"] == null)
                return Value;   // can't compile without required objects; return as received

            string ret = Value;
            MatchCollection col = Regex.Matches(Value, "(?i)\\[\\[[^\\]]*\\]\\]");
            foreach (Match m in col)
            {
                string strTokenName = m.Value.Substring(2, m.Value.Length - 4).ToUpper();
                string strNewValue = GetTokenValue(Context, strTokenName);

                ret = ret.Replace(m.Value, strNewValue);
            }

            return ret;
        }

        private static string GetTokenValue(HttpContext Context, string TokenName)
        {
            // [TESTED: OK]

            // [TODO]
            // -> move to FlowDirector?
            // -> AGE variable

            try
            {

                Hashtable session = Sessions.Visitor;

                switch (TokenName[0])
                {
                    case '$':
                        switch (TokenName.Substring(1).ToUpper())
                        {
                            case "CAMPAIGNCODE":
                                return session["CampaignCode"].ToString();

                            case "REFERRER":
                                return session["Referrer"].ToString();

                            case "SUBID":
                                return session["SubID"].ToString();

                            case "OFFERIMPRESSIONID":
                                return session["OfferImpressionID"].ToString();

                            case "SESSIONID":
                                return session["SessionGUID"].ToString();

                            case "SESSIONUSERID":
                                return session["SessionID"].ToString();

                            case "LOCATIONCODE":
                                return session["LocationCode"].ToString();

                            case "COUNTRYCODE":
                                return new Location(session["LocationCode"].ToString()).CountryCode;

                            case "REGIONCODE":
                                return new Location(session["LocationCode"].ToString()).RegionCode;

                            case "IPADDRESS":
                                return HttpContext.Current.Request.UserHostAddress;

                            case "HOMEPHONENO_NPA":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "HomePhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "HomePhoneNo").Substring(0, 3);
                                }

                            case "HOMEPHONENO_NXX":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "HomePhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "HomePhoneNo").Substring(3, 3);
                                }

                            case "HOMEPHONENO_LAST4":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "HomePhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "HomePhoneNo").Substring(6, 4);
                                }

                            case "CELLPHONENO_NPA":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "CellPhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "CellPhoneNo").Substring(0, 3);
                                }

                            case "CELLPHONENO_NXX":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "CellPhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "CellPhoneNo").Substring(3, 3);
                                }

                            case "CELLPHONENO_LAST4":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "CellPhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "CellPhoneNo").Substring(6, 4);
                                }



                            case "WORKPHONENO_NPA":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "WorkPhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "WorkPhoneNo").Substring(0, 3);
                                }
                            case "WORKPHONENO_NXX":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "WorkPhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "WorkPhoneNo").Substring(3, 3);
                                }

                            case "WORKPHONENO_LAST4":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "WorkPhoneNo").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "WorkPhoneNo").Substring(6, 4);
                                }


                            case "DATEOFBIRTH_YEAR":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "DateOfBirth").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "DateOfBirth").Substring(0, 4);
                                }

                            case "DATEOFBIRTH_MONTH":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "DateOfBirth").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "DateOfBirth").Substring(5, 2);
                                }
                            case "DATEOFBIRTH_DAY":
                                if (FlowDirector.GetLeadValue((Hashtable)session["Lead"], "DateOfBirth").ToString() == "")
                                {
                                    return "";
                                }
                                else
                                {
                                    return FlowDirector.GetLeadValue((Hashtable)session["Lead"], "DateOfBirth").Substring(8, 2);
                                }
                            case "GENDERNUM":

                                string tempGender = FlowDirector.GetLeadValue((Hashtable)session["Lead"], "Gender");
                                Debug.WriteLine("Gender:" + tempGender);
                                if (tempGender == "M")
                                {
                                    return "1";
                                }
                                else
                                {
                                    return "0";
                                }
                        }
                        break;


                    case '%':
                        switch (TokenName.Substring(1).ToUpper())
                        {
                            case "FILEROOT":
                                return ConfigurationManager.AppSettings["HTMLHostingPath"] + "/clientfiles/" + session["ClientID"].ToString();
                        }
                        break;

                    case '^':   // ex: [[^UC:%REGIONCODE]]
                        {
                            Match mFn = Regex.Match(TokenName.Substring(1).ToUpper(), "^([^:]+)");
                            if (!mFn.Success)
                                return "";  // "FN ERROR";

                            MatchCollection mArgs = Regex.Matches(TokenName.Substring(mFn.Value.Length + 2).ToUpper(), "([^,]+)");

                            switch (mFn.Value)
                            {
                                case "UC":  // uppercase
                                    if (mArgs.Count != 1)
                                        return "";  // "UC:INVALID ARGS";
                                    else
                                        return GetTokenValue(Context, mArgs[0].Value).ToUpper();

                                case "LC":  // lowercase
                                    if (mArgs.Count != 1)
                                        return "";  // "LC:INVALID ARGS";
                                    else
                                        return GetTokenValue(Context, mArgs[0].Value).ToLower();

                                default:
                                    return "";  // "UNKNOWN FN";
                            }
                        }

                    case '@':   // offer lead data
                        return FlowDirector.GetLeadValue((Hashtable)session["OfferLead"], TokenName.Substring(1));

                    default:    // LP lead data
                        return FlowDirector.GetLeadValue((Hashtable)session["Lead"], TokenName);
                }
            }
            catch (Exception)
            {
                return String.Empty;
            }
            return String.Empty;
        }

        public static string GetNormalizedGender(object Gender)
        {
            if (Gender == null || Gender.ToString().Length == 0)
                return "";

            switch (Gender.ToString().Substring(0, 1).ToUpper())
            {
                case "M":
                case "F":
                    return Gender.ToString().Substring(0, 1).ToUpper();
                default:
                    return "";
            }
        }

        public static int GetNormalizedIncome(object Income)
        {
            // [UNTESTED]

            int intIncome;
            try {
                intIncome = (int) Income;
            } catch (Exception) {
                try { 
                    intIncome = int.Parse(Income.ToString());
                } catch (Exception) {
                    intIncome = -1;
                }
            }

            return ros.Analytics_GetNormalizedIncome(intIncome);
        }

        public static string GetArrayValue(Hashtable Table, string Field)
        {
            if (Table == null)
                return "";

            return Table[Field.ToLower()] == null ? "" : Table[Field.ToLower()].ToString();
        }

        public static string GetArrayValue(DataRow Row, string Field)
        {
            if (Row == null)
                return "";

            return Row[Field] == null ? "" : Row[Field].ToString();
        }

        public static void Fail()
        {
            throw new Exception("Abort, Retry, Fail?");
        }

        public static bool ValidateURL(string URL)
        {
            // [INCOMPLETE]
            // todo: expand regex
            return Regex.IsMatch(URL, "(?i)^https?://.*$");
        }

        public static bool ValidateHostname(string Hostname)
        {
            // [UNTESTED]
            // todo: expand?
            return Regex.IsMatch(Hostname, "(?i)^[\\w\\d.-]+$");
        }

        public static bool ValidateEmailAddress(string EmailAddress)
        {
            return Regex.IsMatch(EmailAddress, "^[\\w_][\\w._%+-]*@[\\w]([\\w-]+\\.)+[\\w]{2,6}$");
        }

        public static bool ValidateEmailAddresses(string EmailAddresses)
        {
            bool ret = true;
            foreach (string foo in EmailAddresses.Split(','))
            {
                if (!ValidateEmailAddress(foo))
                {
                    ret = false;
                    break;
                }
            }

            return ret;
        }

        public static string EncodeJS(string Text)
        {
            return Text.Replace("\\", "\\\\").Replace("'", "\\'");
        }

        public static string GetBase32UID()
        {
            byte[] g = Guid.NewGuid().ToByteArray();
            byte[] b = new byte[8];
            for (byte i = 0; i < 8; i++)
                b[i] = g[i];

            return new Base32Encoder().Encode(b).Replace("=", "").ToUpper();
            
   
        }

        //public static string B64URLEncode(string Data)
        //{
        //    string ret = Convert.ToBase64String(new ASCIIEncoding().GetBytes(Data));

        //    ret = ret.Replace(".", "-");
        //    ret = ret.Replace("/", "_");
        //    ret = ret.Replace("=", "");

        //    return ret;
        //}

        //public static string B64URLEncode(byte[] Data)
        //{
        //    string ret = Convert.ToBase64String(Data);

        //    ret = ret.Replace(".", "-");
        //    ret = ret.Replace("/", "_");
        //    ret = ret.Replace("=", "");

        //    return ret;
        //}

        //// own implementation of Base64URL encoding
        //public const string B64CODES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
        ////public static string b64encode(string Data)
        ////{
        ////    string ret = "";
        ////    byte[] data = new ASCIIEncoding().GetBytes(Data);

        ////    byte prev = 0, curr;
        ////    int ci = 0;
        ////    int x = 0;
        ////    for (int i = 0; i < data.Length; i++)
        ////    {
        ////        curr = data[i];
        ////        ret += GetB64Code(prev, curr, ci);

        ////        if (i == data.Length - 1 && ci < 2)
        ////            ret += GetB64Code(curr, 0, ci + 1);

        ////        ci++;
        ////        ci %= 3;
        ////        prev = curr;
        ////    }
        ////    return ret;
        ////}

        ////private static string GetB64Code(byte Prev, byte Curr, int StringIndex)
        ////{
        ////    string ret = "";
        ////    int x;
        ////    switch (StringIndex)
        ////    {
        ////        case 0:
        ////            x = (Curr >> 2) & 63;
        ////            ret = B64CODES[x].ToString();
        ////            break;
        ////        case 1:
        ////            x = ((Prev & 3) << 4) + ((Curr & 240) >> 4);
        ////            ret = B64CODES[x].ToString();
        ////            break;
        ////        case 2:
        ////            x = ((Prev & 15) << 2) + ((Curr & 192) >> 6);
        ////            ret = B64CODES[x].ToString();
        ////            x = Curr & 63;
        ////            ret += B64CODES[x].ToString();
        ////            break;
        ////    }
        ////    return ret;
        ////}

        public static string GetKeywordTreeChildren(int ParentKeywordID)
        {
            // todo: move to AdminUtility?
            StringBuilder sb = new StringBuilder();

            DataTable dt = ros.GetChildKeywords(ParentKeywordID);
            if (dt.Rows.Count > 0)
            {
                sb.Append("<ul>");
                foreach (DataRow row in dt.Rows)
                {
                    sb
                        .Append("<li rel=\"")
                        .Append(row["KeywordID"].ToString())
                        .Append("\"><a>")
                        .Append(row["Keyword"].ToString()).Append("</a>")
                        .Append(GetKeywordTreeChildren(Convert.ToInt32(row["KeywordID"])))
                        .Append("</li>");

                }
                sb.Append("</ul>");
            }

            return sb.ToString();
        }

        public static void RegenerateKeywordTree()
        {
            // todo: move to AdminUtility?
            System.IO.File.WriteAllText(
                ConfigurationManager.AppSettings["ResourcePath"] + "keywordtree.html", 
                GetKeywordTreeChildren(int.MinValue));
        }

        public static void RegenerateKeywordData()
        {
            // todo: move to AdminUtility?
            // -> note: instead of comma-delimited, values are pipe-delimited
            DataTable dt = sql.GetDataTable("SELECT * FROM Keywords");

            StringBuilder sb = new StringBuilder();

            foreach (DataRow row in dt.Rows)
            {
                sb  .Append(row["ID"].ToString()).Append("|")
                    .Append("\"").Append(row["Keyword"]).Append("\"");

                string[] words = row["RelatedWords"].ToString().Split('|');
                foreach (string word in words)
                {
                    if (word.Length > 0)
                        sb.Append("|\"").Append(word).Append("\"");
                }

                sb.Append("\r\n");
            }

            System.IO.File.WriteAllText(
                ConfigurationManager.AppSettings["ResourcePath"] + "keyword-data.csv",
                sb.ToString());
        }

        public static string GetSetting(string Key)
        {
            return ConfigurationManager.AppSettings[Key];
        }

        public static string FormatStandardDate(object date)
        {
            return (date == null || date == DBNull.Value) ? "" :
                Convert.ToDateTime(date).ToString(GetSetting("ListDateFormat"));
        }

        public static string QuantifyText(int value)
        {
            return QuantifyText(value, "", "s");
        }

        public static string QuantifyText(int value, string singularText, string pluralText)
        {
            return value == 1 ? singularText : pluralText;
        }

    }
}