﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;


    /// <summary>
    /// Summary description for DB
    /// </summary>
    public class DBCON
    {


        #region Properties

        SqlConnection _sqlCon = new SqlConnection();
        SqlCommand _sqlCmd = new SqlCommand();

        public string ConnectionString
        {
            get { return _sqlCon.ConnectionString; }
            set { _sqlCon.ConnectionString = value; }
        }

        public SqlCommand SqlCommand
        {
            get { return _sqlCmd; }
            set { _sqlCmd = value; }
        }

        #endregion

        #region Constructor

        public DBCON()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultDBConn"].ConnectionString;
            this.SqlCommand.CommandType = CommandType.StoredProcedure;
        }

        public DBCON(string _ConnectionString)
        {
            this.ConnectionString = _ConnectionString;
            this.SqlCommand.CommandType = CommandType.StoredProcedure;
        }

        #endregion

        #region Methods

        public string GetCommandText()
        {
            if (_sqlCmd != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(_sqlCmd.CommandText);
                if (_sqlCmd.Parameters != null)
                {
                    foreach (SqlParameter param in _sqlCmd.Parameters)
                    {
                        sb.AppendLine(param.ParameterName + " : " + param.Value);
                    }
                }
                return sb.ToString();
            }
            else
                return "Command Null";
        }

        public void ResetCommand()
        {
            if (_sqlCmd != null)
            {
                SqlCommand newCmd = new SqlCommand();
                newCmd.CommandText = _sqlCmd.CommandText;

                if (_sqlCmd.Parameters != null)
                {
                    foreach (SqlParameter param in _sqlCmd.Parameters)
                    {
                        newCmd.Parameters.AddWithValue(param.ParameterName, param.Value);
                    }
                }
                _sqlCmd.Dispose();
                _sqlCmd = newCmd;
            }
        }

        public DataTable GetDataTable()
        {
            DataTable dt = new DataTable();
            SqlDataAdapter sqlAdp = new SqlDataAdapter();
            _sqlCmd.Connection = _sqlCon;
            sqlAdp.SelectCommand = _sqlCmd;
            try
            {
                sqlAdp.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("GetDataTable", ex, " Command:" + GetCommandText());
                //throw new Exception(ex.Message + " Command:" + GetCommandText(), ex.InnerException);
            }
            finally
            {
                if (_sqlCmd != null)
                {
                    if (_sqlCmd.Connection != null)
                    {
                        _sqlCmd.Connection.Close();
                        _sqlCmd.Connection.Dispose();
                    }
                    _sqlCmd.Dispose();
                }
                if (sqlAdp != null)
                    sqlAdp.Dispose();
            }
            return dt;
        }

        public DataSet GetDataSet()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter sqlAdp = new SqlDataAdapter();
            _sqlCmd.Connection = _sqlCon;
            sqlAdp.SelectCommand = _sqlCmd;
            try
            {
                sqlAdp.Fill(ds);
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("GetDataSet", ex, " Command:" + GetCommandText());
                //throw new Exception(ex.Message + " Command:" + GetCommandText(), ex.InnerException);
            }
            finally
            {
                if (_sqlCmd != null)
                {
                    if (_sqlCmd.Connection != null)
                    {
                        _sqlCmd.Connection.Close();
                        _sqlCmd.Connection.Dispose();
                    }
                    _sqlCmd.Dispose();
                }
                if (sqlAdp != null)
                    sqlAdp.Dispose();
            }
            return ds;
        }

        public object ExecuteScalar()
        {
            object objScalar = new object();
            _sqlCmd.Connection = _sqlCon;

            try
            {
                _sqlCon.Open();
                objScalar = _sqlCmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("ExecuteScalar", ex, " Command:" + GetCommandText());
                //throw new Exception(ex.Message + " Command:" + GetCommandText(), ex.InnerException);
            }
            finally
            {
                if (_sqlCmd != null)
                {
                    if (_sqlCmd.Connection != null)
                    {
                        _sqlCmd.Connection.Close();
                        _sqlCmd.Connection.Dispose();
                    }
                    _sqlCmd.Dispose();
                }
            }
            return objScalar;
        }

        public Int32 ExecuteNonQuery()
        {
            Int32 i = 0;
            _sqlCmd.Connection = _sqlCon;
            try
            {
                _sqlCon.Open();
                i = _sqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("ExecuteNonQuery", ex, " Command:" + GetCommandText());
                //throw new Exception(ex.Message + " Command:" + GetCommandText(), ex.InnerException);
            }
            finally
            {
                if (_sqlCmd != null)
                {
                    if (_sqlCmd.Connection != null)
                    {
                        _sqlCmd.Connection.Close();
                        _sqlCmd.Connection.Dispose();
                    }
                    _sqlCmd.Dispose();
                }
            }
            return i;
        }


        #endregion


    }
