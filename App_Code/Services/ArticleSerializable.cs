﻿using System;
using System.Data;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

/// <summary>
/// Summary description for BlogSerializable
/// </summary>
public class ArticleSerializable
{
    #region "Private Members"

    private int id;
    private string text;
    private string title;
    private string link;

    #endregion

    [DataMember()]
    public int ID
    {
        get { return id; }
        set { id = value; }
    }

    [DataMember()]
    public string Text
    {
        get { return text; }
        set { text = value; }
    }

    [DataMember()]
    public string Title
    {
        get { return title; }
        set { title = value; }
    }

    [DataMember()]
    public string Link
    {
        get { return link; }
        set { link = value; }
    }

    public ArticleSerializable()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}