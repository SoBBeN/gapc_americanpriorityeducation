﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

///// <summary>
///// Used for WCF services.
///// </summary>
public class FbPageDaySerializable
{

	#region "Private Members"

    private int id;
    private string image;
    private string day;
    private string text;
    private string link;
    private string linkOutside;
    private string title;

	#endregion

    [DataMember()]
    public int ID
    {
        get { return id; }
        set { id = value; }
    }

    [DataMember()]
    public string Image
    {
        get { return image; }
        set { image = value; }
    }

	[DataMember()]
	public string Day {
		get { return day; }
		set { day = value; }
	}

    [DataMember()]
    public string Text
    {
        get { return text; }
        set { text = value; }
    }

    [DataMember()]
    public string Link
    {
        get { return link; }
        set { link = value; }
    }

    [DataMember()]
    public string LinkOutside
    {
        get { return linkOutside; }
        set { linkOutside = value; }
    }

    [DataMember()]
    public string Title
    {
        get { return title; }
        set { title = value; }
    }

    public FbPageDaySerializable()
	{
	}

}
