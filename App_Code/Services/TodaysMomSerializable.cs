﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

/// <summary>
/// Summary description for TodaysMomSerializable
/// </summary>
public class TodaysMomSerializable
{
    #region "Private Members"

    private int id;
    private string image;
    private string image1 = String.Empty;
    private string image2 = String.Empty;
    private string image3 = String.Empty;
    private string logo;
    private string day;
    private string text;
    private string description;
    private string link;
    private string title;
    private string momlink;
    private string linktitle;
    private string facebook;
    private string twitter;
    private string pinterest;

    #endregion

    [DataMember()]
    public int ID
    {
        get { return id; }
        set { id = value; }
    }

    [DataMember()]
    public string Image
    {
        get { return image; }
        set { image = value; }
    }

    [DataMember()]
    public string Image1
    {
        get { return image1; }
        set { image1 = value; }
    }

    [DataMember()]
    public string Image2
    {
        get { return image2; }
        set { image2 = value; }
    }

    [DataMember()]
    public string Image3
    {
        get { return image3; }
        set { image3 = value; }
    }

    [DataMember()]
    public string LinkLogo
    {
        get { return logo; }
        set { logo = value; }
    }

    [DataMember()]
    public string Day
    {
        get { return day; }
        set { day = value; }
    }

    [DataMember()]
    public string Text
    {
        get { return text; }
        set { text = value; }
    }

    [DataMember()]
    public string Link
    {
        get { return link; }
        set { link = value; }
    }

    [DataMember()]
    public string Title
    {
        get { return title; }
        set { title = value; }
    }

    [DataMember()]
    public string Description
    {
        get { return description; }
        set { description = value; }
    }

    [DataMember()]
    public string MomLink
    {
        get { return momlink; }
        set { momlink = value; }
    }

    [DataMember()]
    public string LinkTitle
    {
        get { return linktitle; }
        set { linktitle = value; }
    }

    [DataMember()]
    public string Facebook
    {
        get { return facebook; }
        set { facebook = value; }
    }

    [DataMember()]
    public string Twitter
    {
        get { return twitter; }
        set { twitter = value; }
    }

    [DataMember()]
    public string Pinterest
    {
        get { return pinterest; }
        set { pinterest = value; }
    }

	public TodaysMomSerializable()
	{
	}
}