﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

public class Validation
{

    private StringBuilder errorMsg;
    private string pCustomErrorMessageForNext = string.Empty;

    private bool pAddErrorIfNoLabel = false;

    public Validation()
    {
        errorMsg = new StringBuilder();
    }

    public string ErrorMessage
    {
        get { return errorMsg.ToString(); }
    }

    public bool HasError
    {
        get { return errorMsg.Length > 0; }
    }

    public bool AddErrorIfNoLabel
    {
        get { return pAddErrorIfNoLabel; }
        set { pAddErrorIfNoLabel = value; }
    }

    public string CustomErrorMessageForNext
    {
        set { pCustomErrorMessageForNext = value; }
    }

    public void ErrorMessageAppend(string msg, ref System.Web.UI.WebControls.WebControl lb)
    {
        if (lb.GetType() == typeof(Label))
        {
            Label label = (Label)lb;
            label.Text = msg;
        }
        errorMsg.AppendLine(msg);
    }

    public void PopErrorMessage(System.Web.UI.ClientScriptManager cs)
    {
        cs.RegisterStartupScript(cs.GetType(), "ErrorMsg", "document.body.onload = alert('" + errorMsg.Replace("\r\n", "\\n").Replace("'", "\\'") + "');" , true);
    }

    public bool Firstname(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        return Firstname(text, ref lb, "firstname");
    }

    public bool Firstname(string text, ref System.Web.UI.WebControls.WebControl lb, string fieldName)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if ((!Regex.IsMatch(text, "^[a-zA-Z.\\'\\-\\s]+$")))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, fieldName, ref lb, correction);
        return isValid;
    }

    public bool Lastname(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        return Lastname(text, ref lb, "lastname");
    }

    public bool Lastname(string text, ref System.Web.UI.WebControls.WebControl lb, string fieldName)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if ((!Regex.IsMatch(text, "^[a-zA-Z.\\'\\-\\s]+$")))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, fieldName, ref lb, correction);
        return isValid;
    }

    public bool Email(string text)
    {
        System.Web.UI.WebControls.WebControl lb = null;
        return Email(text, ref lb);
    }

    public bool Email(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if ((!Regex.IsMatch(text, "^[\\.a-zA-Z\\d_\\-]+[\\@][a-zA-Z\\d\\.\\-]+[\\.][\\.a-zA-Z]+$")))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, "email", ref lb, correction);
        return isValid;
    }

    public bool Phone(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        return Phone(text, ref lb, "phone");
    }

    public bool Phone(string text, ref System.Web.UI.WebControls.WebControl lb, string phoneName)
    {
        return Phone(text, ref lb, phoneName, true);
    }

    public bool Phone(string text, ref System.Web.UI.WebControls.WebControl lb, string phoneName, bool allowEmpty)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            correction = CorrectionType.State;
            if (allowEmpty)
            {
                isValid = true;
            }
            else
            {
                isValid = false;
            }
        }
        else
        {
            correction = CorrectionType.Correct;
            if (!(Regex.IsMatch(text, "^[\\d]{10}$")))
            {
                isValid = false;
            }
            else if (!String.IsNullOrEmpty(text))
            {
                if (Convert.ToInt64(text) <= 2001000000)
                {
                    isValid = false;
                }
                else
                {
                    isValid = true;
                }
            }
            else
            {
                return true;
            }
        }

        UpdateMsgAndLabel(isValid, phoneName, ref lb, correction);
        return isValid;
    }

    public bool ZipCode(string text, ref  System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if ((!Regex.IsMatch(text, @"\d{5}(-\d{4})?")))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, "zipcode", ref lb, correction);
        return isValid;
    }

    public bool Occupation(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if ((!Regex.IsMatch(text, "^[a-zA-Z\\'\\-\\s-\\./\\d&,#!]+$")))
            {
                isValid = false;
            }
            else if (!Regex.IsMatch(text, "(?i)(?(?=^.*(''|\\\\.\\\\.|--|aaa|bbb|ccc|ddd|eee|fff|ggg|hhh|iiii|jjj|kkk|lll|mmm|nnn|ooo|ppp|qqq|rrr|sss|ttt|uuu|vvv|www|xxx|yyy|zzz).*$)^$|^([a-z]{2}|[a-z .'\\\\-]*(?<=[aeiouy][bcdfghjklmnpqrstvwxyz]|[bcdfghjklmnpqrstvwxyz][aeiouy])[a-z .'\\\\-]*)$)"))
            {
                isValid = false;
            }
            else if ((new Regex("[^0-9]")).IsMatch((text)))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, "job", ref lb, correction);
        return isValid;
    }


    public bool DesiredJobTitle(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if ((!Regex.IsMatch(text, "^[a-zA-Z.\\'\\-\\s]+$")))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, "Desired job title", ref lb, correction);
        return isValid;
    }

    public bool City(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (String.IsNullOrEmpty(text))
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if ((!Regex.IsMatch(text, "^[a-zA-Z.\\'\\-\\s]+$")))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, "City", ref lb, correction);
        return isValid;
    }

    public bool States(int stateId, ref System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (stateId == 0)
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;

            isValid = true;

        }

        UpdateMsgAndLabel(isValid, "States", ref lb, correction);
        return isValid;
    }

    public bool Resume(int fileLength, ref System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (fileLength == 0)
        {
            isValid = false;
            correction = CorrectionType.Attach;
        }
        else
        {
            correction = CorrectionType.Correct;

            isValid = true;

        }

        UpdateMsgAndLabel(isValid, "Resume", ref lb, correction);
        return isValid;
    }

    public bool NumericTextBox(string text, string fieldName, ref System.Web.UI.WebControls.WebControl lb)
    {
        return NumericTextBox(text, fieldName, ref lb, false, false, decimal.MaxValue, 0);
    }

    //, ref System.Web.UI.WebControls.Label lb = null, bool allowEmpty = false, bool allowDecimal = false, decimal maxValue = decimal.MaxValue, decimal minValue = 0)
    public bool NumericTextBox(string text, string fieldName, ref System.Web.UI.WebControls.WebControl lb, bool allowEmpty, bool allowDecimal, decimal maxValue, decimal minValue)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);

        if (text.Length == 0)
        {
            correction = CorrectionType.State;
            if (allowEmpty)
            {
                isValid = true;
            }
            else
            {
                isValid = false;
            }
        }
        else
        {
            correction = CorrectionType.Correct;
            try
            {
                if (!(new Regex("[^0-9]")).IsMatch(text))
                {
                    isValid = false;
                }
                else if (Convert.ToDecimal(text) > maxValue)
                {
                    isValid = false;
                }
                else if (Convert.ToDecimal(text) < minValue)
                {
                    isValid = false;
                }
                else if (!allowDecimal)
                {
                    if ((!Regex.IsMatch(text, "^[\\d]+$")))
                    {
                        isValid = false;
                    }
                    else
                    {
                        isValid = true;
                    }
                }
                else
                {
                    isValid = true;
                }
            }
            catch (OverflowException)
            {
                isValid = false;
            }
        }

        UpdateMsgAndLabel(isValid, fieldName, ref lb, correction);
        return isValid;
    }

    public bool Dob(string month, string day, string year, ref System.Web.UI.WebControls.WebControl lb)
    {
        return Dob(month, day, year, ref lb, 18);
    }

    public bool Dob(string month, string day, string year, ref System.Web.UI.WebControls.WebControl lb, short age)
    {
        return DateDob(month, day, year, ref lb, "date of birth", age);
    }

    public bool Date(string month, string day, string year, ref System.Web.UI.WebControls.WebControl lb)
    {
        return Date(month, day, year, ref lb, "date");
    }

    public bool Date(string month, string day, string year, ref System.Web.UI.WebControls.WebControl lb, string fieldName)
    {
        return DateDob(month, day, year, ref lb, fieldName);
    }

    public bool Dob(string date, ref System.Web.UI.WebControls.WebControl lb, short age)
    {
        if (date.Length != 10)
        {
            return DateDob(date, string.Empty, string.Empty, ref lb, "date of birth", age);
        }
        else
        {
            return DateDob(date.Substring(0, 2), date.Substring(3, 2), date.Substring(6, 4), ref lb, "date of birth", age);
        }
    }

    public bool Date(string strDate, ref System.Web.UI.WebControls.WebControl lb)
    {
        return Date(strDate, ref  lb, "date");
    }

    public bool Date(string strDate, ref System.Web.UI.WebControls.WebControl lb, string fieldName)
    {
        if (strDate.Length != 10)
        {
            return DateDob(strDate, string.Empty, string.Empty, ref lb, fieldName);
        }
        else
        {
            return DateDob(strDate.Substring(0, 2), strDate.Substring(3, 2), strDate.Substring(6, 4), ref lb, fieldName);
        }
    }

    private bool DateDob(string month, string day, string year, ref System.Web.UI.WebControls.WebControl lb, string fieldName)
    {
        return DateDob(month, day, year, ref lb, fieldName, short.MinValue);
    }

    private bool DateDob(string month, string day, string year, ref System.Web.UI.WebControls.WebControl lb, string fieldName, short age)
    {
        bool isValid = false;
        CorrectionType correction = default(CorrectionType);
        string errorText = string.Empty;

        if (month == null | day == null | year == null)
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else if (month.Length == 0 | day.Length == 0 | year.Length == 0)
        {
            isValid = false;
            correction = CorrectionType.State;
        }
        else
        {
            correction = CorrectionType.Correct;
            if (!((new Regex("\\d+")).IsMatch(day) && (new Regex("\\d+")).IsMatch(month) && (new Regex("\\d+")).IsMatch(year)))
            {
                isValid = false;
            }
            else if (!ValidateDate(Convert.ToInt16(day), Convert.ToInt16(month), Convert.ToInt16(year)))
            {
                isValid = false;
            }
            else if (ValidateAge(Convert.ToInt16(day), Convert.ToInt16(month), Convert.ToInt16(year), age))
            {
                isValid = false;
                fieldName += " (Must be " + age.ToString() + "+)";
            }
            else
            {
                isValid = true;
            }
        }

        UpdateMsgAndLabel(isValid, fieldName, ref lb, correction);
        return isValid;
    }

    public bool Password(string text, string copy, ref System.Web.UI.WebControls.WebControl lb, ref System.Web.UI.WebControls.WebControl lbcopy)
    {
        UpdateMsgAndLabel(true, String.Empty, ref lb, ref lbcopy);

        if (text.Length == 0)
        {
            UpdateMsgAndLabel(false, "password", ref lb, CorrectionType.State);
            return false;
        }
        else if (copy.Length == 0)
        {
            UpdateMsgAndLabel(false, "confirm password", ref lb, CorrectionType.State);
            return false;
        }
        else if (text.Length < 6)
        {
            UpdateMsgAndLabel(false, "password (Must be at least 6 characters)", ref lb, CorrectionType.Correct);
            return false;
        }
        else if (text != copy)
        {
            pCustomErrorMessageForNext = "Please state a password and a confirm password that match";
            UpdateMsgAndLabel(false, String.Empty, ref lb, ref lbcopy);
            return false;
        }
        else
            return true;
    }
    public bool Password(string text, ref System.Web.UI.WebControls.WebControl lb)
    {
        if (text.Length == 0)
        {
            UpdateMsgAndLabel(false, "password", ref lb, CorrectionType.State);
            return false;
        }
        else if (text.Length < 6)
        {
            UpdateMsgAndLabel(false, "password (Must be at least 6 characters)", ref lb, CorrectionType.Correct);
            return false;
        }
        else
        {
            UpdateMsgAndLabel(true, String.Empty, ref lb);
            return true;
        }
    }

    public bool NotEmptyDropDown(string text, string fieldName, ref System.Web.UI.WebControls.WebControl lb)
    {
        return NotEmptyDropDown(text, fieldName, ref lb, CorrectionType.Select);
    }

    public bool NotEmptyDropDown(string text, string fieldName, ref System.Web.UI.WebControls.WebControl lb, CorrectionType correction)
    {
        return NotEmptyTextBox(text, fieldName, ref lb, correction);
    }

    public bool NotEmptyTextBox(string text, string fieldName)
    {
        System.Web.UI.WebControls.WebControl lb = null;
        return NotEmptyTextBox(text, fieldName, ref lb, CorrectionType.State);
    }

    public bool NotEmptyTextBox(string text, string fieldName, ref System.Web.UI.WebControls.WebControl lb)
    {
        return NotEmptyTextBox(text, fieldName, ref lb, CorrectionType.State);
    }

    public bool NotEmptyTextBox(string text, string fieldName, ref System.Web.UI.WebControls.WebControl lb, CorrectionType correction)
    {
        bool isValid = false;
        if (text.Length == 0)
        {
            isValid = false;
        }
        else
        {
            isValid = true;
        }

        UpdateMsgAndLabel(isValid, fieldName, ref lb, correction);
        return isValid;
    }

    public bool CheckedCheckBox(bool isChecked, ref System.Web.UI.WebControls.WebControl lb)
    {
        bool isValid = false;
        if (!isChecked)
        {
            isValid = false;
        }
        else
        {
            isValid = true;
        }

        if (string.IsNullOrEmpty(pCustomErrorMessageForNext))
            pCustomErrorMessageForNext = "Please agree to the following text";
        UpdateMsgAndLabel(isValid, String.Empty, ref lb);
        return isValid;
    }

    private bool ValidateDate(short day, short month, short year)
	{
		if (month < 0 | month > 13) {
			return false;
		} else if (day < 29) {
			return true;
        }
        else if (day <= DateTime.DaysInMonth(year, month))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool ValidateAge(short day, short month, short year, short age)
    {
        if ((year < (System.DateTime.Now.Year - age)))
        {
            return false;
        }
        else if ((year > (System.DateTime.Now.Year - age)))
        {
            return true;
            //If (year = (Date.Now.Year - age)) Then
        }
        else
        {
            if ((month < System.DateTime.Now.Month))
            {
                return false;
            }
            else if ((month > System.DateTime.Now.Month))
            {
                return true;
                //If (month = Date.Now.Month) Then
            }
            else
            {
                if ((day < System.DateTime.Now.Day))
                {
                    return false;
                }
                else if ((day > System.DateTime.Now.Day))
                {
                    return true;
                }
                else
                {
                    return true;
                }
            }
        }
    }

    private void UpdateMsgAndLabel(bool isValid, string fieldname, ref System.Web.UI.WebControls.WebControl lb)
    {
        UpdateMsgAndLabel(isValid, fieldname, ref lb, CorrectionType.Correct);
    }

    private void UpdateMsgAndLabel(bool isValid, string fieldname, ref System.Web.UI.WebControls.WebControl lb, CorrectionType correction)
    {
        if (isValid)
        {
            if ((lb != null))
            {
                if (lb.GetType() == typeof(Label))
                {
                    Label label = (Label)lb;
                    label.Text = String.Empty;
                }
                lb.BorderColor = Color.Empty;
                lb.ForeColor = Color.Empty;
            }
        }
        else
        {
            bool lblVisible = false;
            if ((lb != null))
            {
                if (lb.GetType() == typeof(TextBox))
                    lb.BorderColor = Color.Red;
                else
                    lb.ForeColor = Color.Red;
                lblVisible = lb.Visible;
            }
            else
            {
                lblVisible = false;
            }

            if (lblVisible | pAddErrorIfNoLabel)
            {
                if (!String.IsNullOrEmpty(pCustomErrorMessageForNext))
                {
                    ErrorMessageAppend(pCustomErrorMessageForNext, ref lb);
                    pCustomErrorMessageForNext = string.Empty;
                }
                else
                {
                    string verb = null;
                    switch (correction)
                    {
                        case CorrectionType.State:
                            verb = "enter";
                            break;
                        case CorrectionType.Correct:
                            verb = "correct";
                            break;
                        case CorrectionType.Select:
                            verb = "select";
                            break;
                        case CorrectionType.Describe:
                            verb = "describe";
                            break;
                        case CorrectionType.Attach:
                            verb = "attach";
                            break;
                        default:
                            verb = "correct";
                            break;
                    }

                    ErrorMessageAppend("Please " + verb + " your " + fieldname + ".", ref lb);
                }
            }
        }
    }

    private void UpdateMsgAndLabel(bool isValid, string fieldname, ref System.Web.UI.WebControls.WebControl lb, ref System.Web.UI.WebControls.WebControl lb2)
    {
        UpdateMsgAndLabel(isValid, fieldname, ref  lb, ref lb2, CorrectionType.Correct);
    }

    private void UpdateMsgAndLabel(bool isValid, string fieldname, ref System.Web.UI.WebControls.WebControl lb, ref System.Web.UI.WebControls.WebControl lb2, CorrectionType correction)
    {
        UpdateMsgAndLabel(isValid, fieldname, ref lb, correction);
        if (isValid)
        {
            if ((lb2 != null))
            {
                lb2.BorderColor = Color.Empty;
                lb2.ForeColor = Color.Empty;
            }
        }
        else
        {
            if ((lb2 != null))
            {
                if (lb2.GetType() == typeof(TextBox))
                    lb2.BorderColor = Color.Red;
                else
                    lb2.ForeColor = Color.Red;
            }
        }
    }

    public enum CorrectionType
    {
        State = 0,
        Correct = 1,
        Select = 2,
        Describe = 3,
        Attach
    }
}