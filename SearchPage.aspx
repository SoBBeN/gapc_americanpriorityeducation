﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="SearchPage.aspx.cs" Inherits="SearchPage" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server" EnableViewState="True" ViewStateMode="Inherit">
    <link href="css/search.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/paging_style.css" media="screen"/>
    <script language="javascript" type="text/javascript" src="js/json2.js"></script>

    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <br />
    <div class="holder_content" >
        <div id="searchresults">
            <div class="BottomSearch">
                <div style=" float:right; margin-top: 9px; margin-right: 15px;">
                     <asp:TextBox runat ="server" ID="TxtBoxSearch" Width="200px" style="margin-right: 5px;" />
                     <asp:Button runat="server" ID="btnSearch" Text="Search" Width="89px"></asp:Button>
               </div>
            </div>
            <div style="margin-bottom: 100px;">
               <div id="pagination" class="demo">
                    <asp:Label runat="server" ID = "LblResult" />
               </div>
            </div>
        </div>
        <asp:Label runat="server" ID = "ScriptLabel" />  
    </div>
    <br /><br /><br /><br /><br />
</asp:Content>

    




