﻿using NSS.ROS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class jobs : System.Web.UI.Page
{
    const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        string IpAddress = Request.UserHostAddress;
        string _q, _l;
        int start;
        int page;
        string api_key = "sn8v7e8t7527g9i3n5yiaye4hdij4pnt";

        if (Page.IsPostBack)
        {
            _q = title.Value;
            _l = location.Value;
            start = 0;
            page = 1;
        }
        else
        {
            if (!String.IsNullOrEmpty(Request.QueryString["title"]))
            {
                _q = Request.QueryString["title"];
                title.Value = _q;
            }
            else
                _q = String.Empty;

            if (!String.IsNullOrEmpty(Request.QueryString["location"]))
            {
                _l = Request.QueryString["location"];
                location.Value = _l;
            }
            else
            {
                Hashtable lead = FlowDirector.CurrentLead;
                if (lead != null && lead.ContainsKey("postalcode"))
                {
                    _l = Convert.ToString(lead["postalcode"]);
                    location.Value = _l;
                }
                else
                {
                    _l = String.Empty;
                }
            }

            if (int.TryParse(Request.QueryString["page"], out page))
                start = (page - 1) * NB_PER_PAGE;
            else
            {
                start = 0;
                page = 1;
            }
        }

        if (_l.Length == 0)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc = Functions.GetLocationInfoByIPAddress(IpAddress);

            if (xmlDoc.SelectSingleNode("//Response/City") != null && xmlDoc.SelectSingleNode("//Response/RegionName") != null)
            {
                if (xmlDoc.SelectSingleNode("//Response/City").InnerText == "" || xmlDoc.SelectSingleNode("//Response/RegionName").InnerText == "")
                {
                    _l = xmlDoc.SelectSingleNode("//Response/City").InnerText;

                    if (_l.Length == 0)
                    {
                        _l = xmlDoc.SelectSingleNode("//Response/RegionName").InnerText;
                    }
                }
                else
                {
                    _l = xmlDoc.SelectSingleNode("//Response/City").InnerText + ", " + xmlDoc.SelectSingleNode("//Response/RegionName").InnerText;
                }
            }
        }

       var sb = new System.Text.StringBuilder();
        sb.Append("https://api.ziprecruiter.com/jobs/v1?api_key=" + api_key);
        sb.Append("&search=").Append(Server.UrlEncode(_q));
        sb.Append("&location=").Append(Server.UrlEncode(_l));
        sb.Append("&radius_miles=25&days_ago=");
        sb.Append("&jobs_per_page=").Append(NB_PER_PAGE);
        sb.Append("&page=").Append(page);

        String url = sb.ToString();
        String response;

        try
        {
            response = Functions.DoGet(ref url);

            if (response != string.Empty)
            {
                dynamic dyn = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                int totalresults = Convert.ToInt32(dyn.total_jobs.ToString());

                if (bool.Parse(dyn.success.ToString()))
                {
                    sb = new StringBuilder();
                    int i = 0;
                    foreach (var obj in dyn.jobs)
                    {

                        sb.Append("<div class=\"job\">");
                        sb.AppendFormat("<a target=\"_blank\" href=\"{0}\">{1}</a>", obj.url.ToString(), obj.name.ToString());
                        sb.AppendFormat("<div class=\"job_location\">{0}</div>", obj.location.ToString());
                        sb.AppendFormat("<div class=\"job_desc\">{0}</div>", obj.snippet.ToString());
                        sb.Append("</div>");

                               i++;
                    }

                    litJobs.Text = sb.ToString();

                    int itemsperpage = 6;
                    int pageNumber = (start) / itemsperpage + 1;

                    DoPaging(start, pageNumber, totalresults, itemsperpage, "title=" + Server.UrlEncode(_q) + "&location=" + Server.UrlEncode(_l) + "&");
                }

                litTitle.Text = "Jobs in " + _l;
            }
        }
        catch (Exception ex)
        {
            ErrorHandling.SendException("DoGet ACO Juju", ex, url);
            return;
        }

    }
    private void DoPaging(int intStart, int currentPage, int recordCount, int pageSize, string baseVars)
    {
        int intPageCount = 0;

        intPageCount = (int)Math.Ceiling((double)recordCount / (double)pageSize);

        int intEnd = intStart + pageSize - 1;
        if (intEnd > recordCount)
        {
            intEnd = recordCount;
        }

        StringBuilder html = new StringBuilder();

        if (recordCount < 10)
        {
            html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + intEnd + " of " + recordCount + "</div>");
        }
        else if (currentPage == intPageCount)
        {
            if (Convert.ToBoolean(recordCount % 10))
            {
                html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + intEnd + " of " + recordCount + "</div>");
            }
            else
            {
                html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + (intEnd + 1) + " of " + recordCount + "</div>");
            }

        }
        else
        {
            html.AppendLine("<div class=\"pager-count\">Displaying " + (intStart + 1) + "-" + (intEnd + 1) + " of " + recordCount + "</div>");
        }

        html.AppendLine("<div class=\"paging\">");
        html.AppendLine("<div class=\"paging_left\">");

        if ((currentPage != 1))
        {
            //html.AppendLine("<a class=\"pager-a\" href=\"ACG_Indeed.aspx?" + baseVars + "page=1\">First</a>&nbsp;");
            html.AppendLine("<a class=\"pager-a\" href=\"jobs.aspx?" + baseVars + "page=" + (currentPage - 1) + "\">Previous</a>&nbsp;&nbsp;");
        }

        html.AppendLine("</div><div style=\"display: inline-block;\"><ul class=\"ul-pager\">");

        int pageStart, pageEnd;
        if (currentPage - 4 < 1)
            pageStart = 1;
        else
            pageStart = currentPage - 4;
        if (currentPage + 4 > intPageCount)
            pageEnd = intPageCount;
        else
            pageEnd = currentPage + 4;

        for (int i = pageStart; i <= pageEnd; i++)
        {
            if (i == currentPage)
            {
                html.AppendLine("<li class=\"selected\">" + i.ToString() + "</li>");
            }
            else
            {
                html.AppendLine("<li><a href=\"jobs.aspx?" + baseVars + "page=" + i + "\">" + i + "</a></li>");
            }
        }
        html.AppendLine("</ul></div>");
        html.AppendLine("<div class=\"paging_right\">");

        if ((currentPage < intPageCount))
        {
            html.AppendLine("&nbsp;&nbsp;<a class=\"pager-a\" href=\"jobs.aspx?" + baseVars + "page=" + (currentPage + 1) + "\">Next</a>");
        }
        html.AppendLine("</div>");
        html.AppendLine("</div>");
        litPaging.Text = html.ToString();

    }
}