﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageRegister.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


		

        <!-- Bootstrap CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet">
      	<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600" rel="stylesheet">

        <!-- Animate.css -->
        <link href="/css/animate.css" rel="stylesheet">
      
      	<!-- Font Awesome -->
      	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

          	<link href="/css/base.css" rel="stylesheet" />

        	<!-- jQuery -->           
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
      	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
        <script type="text/javascript" src="https://d3v7hbq4afry8x.cloudfront.net/js/jquery.mask.min.js"></script>
        <script src="/js/validator.js"></script>
    <script type="text/javascript" src="/js/moment.js"></script>
    
        <!-- Custom CSS -->
       	<style>
            
body {
    background-image: url(../images/APE_bg.jpg);
  background-position: center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
    background-attachment: fixed;
    background-repeat: no-repeat;
    
}


    /* Custom, iPhone Retina */ 
    @media only screen and (min-width : 320px) {
        #acgLogo {    
            width: 80px;
            padding-top: 6%;
        }
        h2 {
            font-size: 1.0em;
            font-family: 'Kanit', sans-serif;
            color: #ffffff;
        }
        body {
            margin-bottom: 62px !important;
        }
    }

    /* Extra Small Devices, Phones */ 
    @media only screen and (min-width : 480px) {
        #acgLogo {    
            width: 80px;
            padding-top: 6%;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        #acgLogo {
            width: 150px;
            padding-top: 2%;
        }
        h2 {
            font-family: 'Open Sans', sans-serif;
            color: #ffffff;
            font-size: 1.5em;
        }
    }

       	</style>
            <script>
         $(document).ready(function () {
        $('#txt_phone').mask('(000) 000-0000');
      });
  </script>
  
      
                       <!-- Bootstrap JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      


        <style>         
          
    .error {
            color: red;
            border: unset !important;
            text-align: left;
            font-size: 14px;
            font-family: sans-serif;
            font-weight: 500;
        }
   .form-control {
      padding: 1rem;
      box-shadow: 0px 0px 4px black;
      width: 95%;
      margin: auto;
      margin-top: 16px;
   }       
          
    .subcopy {
padding-bottom: 0;
    font-size: 44px;
    color: #E59A13;
    font-family: 'kanit','Open Sans', sans-serif;
    font-weight: 600;
    text-align: center;
    line-height: 1.1;
    margin-bottom: 0;
    }
     
    #subtitle {
      font-size: 40px;
      font-weight: 500;
      margin-top: 0px;
      margin-bottom: 20px; 
      text-align: center; 
      color: #ffffff;
      font-family: 'kanit','Open Sans', sans-serif;
    }
img#logo {
    width: 50%;
    padding-top: 2%;
    padding-bottom: 2%;
    max-width: 425px;
}
  footer.footer {
    margin-top: 10%;
    color: black;
    font-family: 'Kanit','Open Sans', sans-serif;
    font-weight: 300;
    color: #ffffff;
}
  footer .legal{
  	width: 95%;
    margin: 0 auto 10px;
    font-weight: 300;
    line-height: 1.1;
    font-size: 14px;
  }
    footer.footer a {
    font-family: 'Kanit','Open Sans', sans-serif;
    font-weight: 300;
    color: #ffffff;
}
                 
/*question css start*/
   .PFG_SingleLayout_1203 .sweeps{
    display:none;
   }     
   .PFG_SingleLayout_1203 .form_bg {
    background: rgba(255, 255, 255, 0.6);
    padding: 3%;
    width:95%;
    max-width:700px;
    margin-top: 2%;
    margin-bottom: 2%;
    display: inline-block;
    border-radius: 15px;
}   
.PFG_SingleLayout_1203 .coreg_buttons > a {
  color:white;
  background-color: #49882C; 
  text-decoration:none;
  font-family: 'kanit','Open Sans', sans-serif;
  width: calc(50% - 30px);
  padding: 15px;
  transition-duration: 0.4s;
}
.PFG_SingleLayout_1203 .coreg_buttons > a:hover {
          box-shadow: 0px 3px 20px #1c1c1c;
}
.PFG_SingleLayout_1203 .coreg_question {
	font-family: 'kanit','Open Sans', sans-serif;
    font-weight: 600;
    font-size: 25px;
}
.PFG_SingleLayout_1203 .draw_footer_container {
  	font-family: 'kanit','Open Sans', sans-serif;
    color: #ffffff;
    margin-top: 12%;
    color: black;
}
          
/*question css end*/
@media(max-width:768px){
  .subcopy {
    font-size: 35px;
  }
  #subtitle {
    font-size: 30px;
  } 
  .PFG_SingleLayout_1203 .coreg_buttons > a {
    width: calc(50% - 10px);
	}
    .PFG_SingleLayout_1203 .coreg_buttons > a {
      margin: 10px 5px;
	}
}
@media (max-width:500px) { 
    	footer.footer {
    margin-top: 5%;
}
    footer .legal {
    font-size: 12px;
}
    footer.footer a {
     font-size: 12px;
	}
   .subcopy {
     font-size: 25px;
   }
  #subtitle {
    font-size: 22px;
  }
} 
          </style>
      
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">

     <header>
          
         <div class="container">
      <div class="logo text-center">
                  <img src="/images/apelogo.png" id="logo">
          </div>
       </div>
<%--       <div class="container" id="head">
              <h1 class="questions subcopy" id="head">SIGN UP AND MATCH WITH THE RIGHT JOB!</h1>
    <!--  <h2 id="subtitle">Get approved within 24 hours!</h2> -->
         <br />
        </div>--%>
    
          
        </header>
    <style>
        header {
        background-color: #04294f;
        }
    #skipButton{
   display:none;
  }
   footer .legal {
  	display:none;
  }
   .form_container
  {
    background: rgba(255, 255, 255, 0.6);
    margin-right: auto;
    margin-left: auto;
    margin-top: 3%;
    width: 95%;
    max-width: 460px;
    border-radius: 20px;
    padding:20px 10px;

  }
    #consent{
        padding-right:20px;
    }
    
    .disclaimertext{
    text-align: center;
    font-size: 9pt;
    padding-top: 10px;
    }
   .form-control {
    padding: 1rem;
    box-shadow: 0px 0px 4px black;
    width: 95%;
    max-width: 360px;
    margin: auto;
    margin-top: 16px; 
}
     #txt_email {
    margin-top: 0px; 
}
   Select {
padding: 1rem;
    box-shadow: 0px 0px 4px black;
    margin: auto;
    margin-top: 16px;
    width: calc(33% - 3px);
  }

   .selectWrapper{
      width: 95%;
    max-width: 360px;
    margin: auto;
  }
 input#btnSubmit {
background-color: #04294f;
    color: #ffffff;
    font-family: 'Open Sans', sans-serif;
    font-weight: 700;
    font-size: 36px;
    border-color: #1075c1;
    text-align: center;
    display: block;
    max-width: 360px;
    width: 95%;
    margin: 10px auto;
}

      #disclaimer_1 {
        float: left;
        margin-top: 5%;
     }

  @media(max-width:500px){
  	 input#btnSubmit {
    	font-size: 27px;
	}
     .form-control {
    	padding: .75rem;
	}
     Select {
    	padding: .75rem;
    }
  }
    @media(max-width:375px){
  	 input#btnSubmit {
    	font-size: 24px;
	}
  }
</style>

 
<div class="container">
<!-- multistep form -->
<div class="form_container uniform_margin">
<form id="msform" class="signupForm" method="post" action="/thankyou.aspx">
    <div class="inner">
                            <div class="input_wrapper_outer">
                                <div class="input_wrapper">
  <!--Needed-->
  <input type="hidden" name="remotePenCriteriaPassed" id="remotePenCriteriaPassed" value="false" />
  <input type="hidden" name="zip_lookup_done" id="zip_lookup_done" value="" />
  <input type="hidden" name="remotePenCheckEmail" id="remotePenCheckEmail" value="" />

  <!-- fieldsets -->
  <fieldset>
   
<%--      <div class="inner_input">
    <input type="text" name="email" id="txt_email" value="" placeholder="Email" class="form-control" autofocus="autofocus" required />
    <div class="help-block with-errors text-center">Please enter your email</div>
    </div>--%>
      
    <div class="inner_input">
    <input type="text" name="first_name" id="txt_first_name" value="" placeholder="First Name" class="form-control"  />
    </div>
 <%--     
   	<div class="inner_input">
    <input type="text" name="last_name" id="txt_last_name" value="" placeholder="Last Name" class="focus form-control" required/>
      <div class="help-block with-errors text-center">Please enter your last name</div> 
    </div>
    <div class="inner_input">
    <input type="tel" name="zip" id="txt_zip" value="" placeholder="Zip Code" class="focus form-control" required />
    <div class="help-block with-errors text-center">Please enter a valid zip code</div>
    </div>
    <div class="inner_input">
    	<input type="text" name="address_1" id="txt_address" value="" placeholder="Address" class="focus form-control" required />
    	<div class="help-block with-errors text-center">Please enter a valid address</div>
    </div>
    <div class="inner_input">
      <div class="selectWrapper">
      <select name="dob_m" id="ddl_dob_month" class="slide_dob">
        <option selected="selected" value="">Date</option>
        <option value="01">Jan</option>
        <option value="02">Feb</option>
        <option value="03">Mar</option>
        <option value="04">Apr</option>
        <option value="05">May</option>
        <option value="06">Jun</option>
        <option value="07">Jul</option>
        <option value="08">Aug</option>
        <option value="09">Sep</option>
        <option value="10">Oct</option>
        <option value="11">Nov</option>
        <option value="12">Dec</option>
      </select>

      <select name="dob_d" id="ddl_dob_day" class="slide_dob">
        <option selected="selected" value="">Of</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option> 
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
      </select>
      <select name="dob_y" id="ddl_dob_year" class="slide_dob"></select>
        </div>
      </div>
       <div class="inner_input">
      <input type="tel" name="dob" class="focus" style="display:none;" id="hdn_dob" value="" placeholder="(mm/dd/yyyy)" required>
      <div class="help-block with-errors text-center">Please enter a valid birthday</div>  
      
    </div>--%>
    <div class="inner_input">
    	<input type="tel" name="phone" id="txt_phone" value="" placeholder="(123) 456-7890" class="focus form-control" />
    </div>  
        <div class="row">
  	<div class="col-lg-12">
                    <input type="checkbox" name="checkbox_1" id="disclaimer_1" value="" />
            
                <asp:Literal runat="server" ID="litDisclaimer1"></asp:Literal>
          <div class="checkbox_wrapper"></div>
                                      <br />
                                      <asp:Literal runat="server" ID="litDisclaimer2"></asp:Literal>
           </div>
  </div>
  

  </fieldset>
                              </div>
      </div>
  </div>
 <table class="record_table">
    <tr>
        <td id="consent"><input type="checkbox" name="checkbox_2" id="opt_in" value="true" /></td>
        <td id="consentInfo">I Confirm that all of my information is accurate and consent to be contacted as provided above</td>
    </tr>
</table> 
    <input type="submit" id="btnSubmit" class="submit submitForm submit-btn" value="Submit" />
</form>

       
      
</div>
 
</div>
      <script type="text/javascript">
  $(function(){
    

    
      $('.signupForm').validate(
      {
      rules:{
         first_name:
                  {
                     required: true,
                     fNameRegex: true
                    },
             phone:
                {
                     required: true,
                     phoneRegex: true
          },
          checkbox_1:
          {
              required: true
          },

        
        

            },
  messages:{
         first_name:
                  { 
                      required: "First Name is required.",
                      fNameRegex: "Please Enter a Valid First Name"
                    },
          phone:
                    {
                      required: "Phone is required.",
                      phoneRegex: "Please Enter a Valid Phone Number"
                     },
                    checkbox_1:
                      {
                          required: "Please agree to the Terms and Privacy to continue"
                      },
              },
              submitHandler: function (form) {
                    //check if phone number field exist
                    if($('#txt_phone').length > 0)
                    {
                      //unmask phone number if it exist before submit
                      $('#txt_phone').unmask();
                    }
                 //disable submit button
                  $('input[type="submit"]').attr('disabled','disabled');
                  console.log($('#txt_phone').val());
                  form.submit();
              },
               errorPlacement: function(error, element) 
        {
            if ( element.is(':checkbox') ) 
            {
                console.log(error);
                    error.appendTo(".checkbox_wrapper");
            }
            else 
            { // This is the default behavior 
                error.insertAfter(element);
            }
        },


      });

        //first_name 
            $.validator.addMethod("fNameRegex",function(value,element)
            {
              return /^[a-zA-Z.\\'\\-\\s]+$/.test(value);
            });
          //phone
            $.validator.addMethod("phoneRegex",function(value,element)
            {
                return /^\(\d{3}\)\s?\d{3}-\d{4}$/.test(value);
            });

  }); 
  
 
</script>


    
    	<!-- Footer -->	
    <footer class="footer">
                  <!-- Skip Button -->
        <div class="row" id="skipButton" class="">
            <div class="col-md-12">
                <a onclick="clickCounter()" href="/api/offers"><h4 class="skip" style="color: #C6C4C1;float: right;padding-right: 20px;">Skip &nbsp;<i style="color:#C6C4C1;" class="fa fa-arrow-circle-right"></i></h4></a>
            </div>
        </div>    
         <div class="container text-center">
           Copyright &copy;
    		<span id="copyright">
        		<script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
    		</span>
   			American Priority Education, LLC 
          <a href="/Terms.aspx" target="_blank">Terms and Conditions</a> | <a href="/Privacy.aspx" target="_blank">Privacy Policy</a> | <a href="/Unsub.aspx" target="_blank">Unsubscribe</a>
        </div>  
    </footer>

    
    </asp:Content>