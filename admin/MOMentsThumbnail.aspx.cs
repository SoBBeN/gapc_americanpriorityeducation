﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MOMentsThumbnail : System.Web.UI.Page
{
    protected string sourcePath;
    protected string sourceFolder;
    protected string type;
    protected string ratio = "1";

    protected void Page_Init(object sender, EventArgs e)
    {
        //sourcePath =  HttpContext.Current.Server.MapPath("../Images/Images/");
        if (!String.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "dd")
            type = "dd";
        else if (!String.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "pd")
            type = "pd";
        else if (!String.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "lf")
        {
            type = "lf";
            ratio = "1.77";
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "nw")
            type = "nw";
        else if (!String.IsNullOrEmpty(Request.QueryString["type"]) && Request.QueryString["type"] == "blg")
            type = "blg";
        else
            type = "mm";
        if (type == "dd")
        {
            sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"];
            sourceFolder = "images/coupons";
        }
        else if (type == "pd")
        {
            sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagesproducturl"];
            sourceFolder = "images/product";
        }
        else if (type == "lf")
        {
            sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"];
            sourceFolder = "images/lifestyle";
        }
        else if (type == "nw")
        {
            sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "news/";
            sourceFolder = "images/news";
        }
        else if (type == "blg")
        {
            sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "blogs/";
            sourceFolder = "images/blogs";
        }
        else
        {
            sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "Images/";
            sourceFolder = "images/images";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string sourceFile = Request.QueryString["f"];

        if (type == "dd")
            imgCropbox.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"];
        else if (type == "pd")
            imgCropbox.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesproducturl"];
        else if (type == "lf")
            imgCropbox.Src = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"];
        else if (type == "nw")
            imgCropbox.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "news/";
        else if (type == "blg")
            imgCropbox.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "blogs/";
        else
            imgCropbox.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "Images/";

        imgCropbox.Src += sourceFile + "?r=" + Guid.NewGuid().ToString();
        imgCropbox.Attributes.Add("onload", "updateDivWidth(this.width, this.height, this);");
    }


    // Source : http://stackoverflow.com/questions/2163829/how-do-i-rotate-a-picture-in-c-sharp

    
    protected void btnRotate_ClickLeft(object sender, EventArgs e)
    {
        Rotate(RotateFlipType.Rotate270FlipNone);
    }
    protected void btnRotate_ClickRight(object sender, EventArgs e)
    {
        Rotate(RotateFlipType.Rotate90FlipNone);
    }

    protected void Rotate(RotateFlipType angle)
    {
        string ImageName = Request.QueryString["f"];

        byte[] CropImage = Rotate(Path.Combine(sourcePath, ImageName), angle);

        if (CropImage != null)
        {
            var azureStorage = new AzureStorage("AzureStorageConnection");
            var blob = azureStorage.UploadBlog("themommyguide", sourceFolder, ref ImageName, CropImage, true);
        }
    }

    static byte[] Rotate(string Img, RotateFlipType angle)
    {
        //try
        //{
        System.Net.WebClient wc = new System.Net.WebClient();
        byte[] bytes = wc.DownloadData(Img);
        MemoryStream ms = new MemoryStream(bytes);

        using (System.Drawing.Image OriginalImage = System.Drawing.Image.FromStream(ms))
        {
            System.Drawing.Imaging.ImageFormat oldFormat = OriginalImage.RawFormat;
            OriginalImage.RotateFlip(angle);

            using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(OriginalImage.Width, OriginalImage.Height))
            {
                bmp.SetResolution(OriginalImage.HorizontalResolution,
                  OriginalImage.VerticalResolution);

                using (System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp))
                {
                    Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                    Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    Graphic.DrawImage(OriginalImage, new Point(0,0));
                    ms = new MemoryStream();
                    bmp.Save(ms, oldFormat);
                    return ms.GetBuffer();
                }
            }
        }
        //}

        //catch (Exception Ex)
        //{
        //    throw (Ex);
        //}
    }




    // Source : http://stackoverflow.com/questions/10446621/asp-net-jcrop-value-issue
    protected void btnCrop_Click(object sender, EventArgs e)
    {
        string sourceFile = Request.QueryString["f"];
        int imageid = Convert.ToInt32(Request.QueryString["id"]);

        //File.Move(Path.Combine(sourcePath, sourceFile), Path.Combine(sourcePath, destinationFile));

        string thumbnailFile;

        float w;
        float h;
        float x;
        float y;

        if (float.TryParse(W.Value, out w) && float.TryParse(H.Value, out h) && float.TryParse(X.Value, out x) && float.TryParse(Y.Value, out y))
        {
            if (String.IsNullOrEmpty(Request.QueryString["t"]))
                thumbnailFile = Functions.GetNonExistingFilename(sourcePath, "t" + sourceFile);
            else
                thumbnailFile = Request.QueryString["t"];

            byte[] CropImage = Crop(Path.Combine(sourcePath, sourceFile), (int)Math.Round(w, 0), (int)Math.Round(h, 0), (int)Math.Round(x, 0), (int)Math.Round(y, 0));

            if (CropImage != null)
            {
                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", sourceFolder, ref thumbnailFile, CropImage, true);
            }
        }
        else
        {
            thumbnailFile = null;
        }

        if (type == "dd")
        {
            DB.DbFunctions.UpdatePointsOffersThumbnail(imageid, thumbnailFile);
            Response.Redirect("PointsOffersAdd.aspx?id=" + imageid.ToString());
        }
        else if (type == "pd")
        {
            DB.DbFunctions.UpdateProductThumbnail(imageid, thumbnailFile);
            Response.Redirect("ProductAdd.aspx?id=" + imageid.ToString());
        }
        else if (type == "lf")
        {
            DB.DbFunctions.UpdateLifestyleThumbnail(imageid, thumbnailFile);
            Response.Redirect("LifestyleSlideAdd.aspx?id=" + imageid.ToString());
        }
        else if (type == "blg")
        {
            DB.DbFunctions.UpdateBloggerThumbnail(imageid, thumbnailFile);
            Response.Redirect("BloggersAdd.aspx?id=" + imageid.ToString());
        }
        else if (type == "nw")
        {
            DB.DbFunctions.UpdateBloggerThumbnail(imageid, thumbnailFile);
            Response.Redirect("NewsAdd.aspx?id=" + imageid.ToString());
        }
        else
        {
            DB.DbFunctions.UpdateImage(imageid, thumbnailFile);
            Response.Redirect("MOMentsAdd.aspx?id=" + imageid.ToString());
        }
    }

    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        //try
        //{
        System.Net.WebClient wc = new System.Net.WebClient();
        byte[] bytes = wc.DownloadData(Img);
        MemoryStream ms = new MemoryStream(bytes);

        using (System.Drawing.Image OriginalImage = System.Drawing.Image.FromStream(ms))
        {
            using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(Width, Height))
            {
                bmp.SetResolution(OriginalImage.HorizontalResolution,
                  OriginalImage.VerticalResolution);

                using (System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp))
                {
                    Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                    Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    Graphic.DrawImage(OriginalImage,
                      new System.Drawing.Rectangle(0, 0, Width, Height),
                      X, Y, Width, Height, System.Drawing.GraphicsUnit.Pixel);
                    ms = new MemoryStream();
                    bmp.Save(ms, OriginalImage.RawFormat);
                    return ms.GetBuffer();
                }
            }
        }
        //}

        //catch (Exception Ex)
        //{
        //    throw (Ex);
        //}
    }
}