﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ImageGalleryAdd.aspx.cs" Inherits="ImagesAdd" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFile" AssociatedControlID="ImageFile" Text="Image File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="ImageFile" /><br />
                    <asp:TextBox ID="txtUrl" runat="server" Text="http://" Width="400"></asp:TextBox><br />

                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Enter value in at least one box" OnServerValidate="CustomValidator_ServerValidate"></asp:CustomValidator>
                </td>
                <div style="float:right; overflow:visible; height:0px; position:relative; right:0px; top:-50px;">
                    <img runat="server" id="imgImageFile" style="max-width:340px;" />
                </div>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    <asp:Label runat="server" ID="lblSize" AssociatedControlID="txtWidth" Text="New Size:<br />(Optional)" />
                </td>
                <td>
                    <asp:TextBox ID="txtWidth" runat="server" CssClass="text" Columns="5" Width="150" MaxLength="5" placeholder="Width" />px&nbsp;
                    <asp:CompareValidator ID="cv" runat="server" ControlToValidate="txtWidth" Type="Integer" Operator="DataTypeCheck" ErrorMessage="Value must be an integer!" /><br />
                    <asp:TextBox ID="txtHeight" runat="server" CssClass="text" Columns="5" Width="150" MaxLength="5" placeholder="Height (Optional)" />px&nbsp;
                    <asp:CompareValidator ID="cv2" runat="server" ControlToValidate="txtHeight" Type="Integer" Operator="DataTypeCheck" ErrorMessage="Value must be an integer!" />
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" />
    </div>
</asp:Content>

