﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdminsAdd.aspx.cs" Inherits="AdminsAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblUsername" AssociatedControlID="txtUsername" Text="Username:" />
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server" CssClass="text" Columns="16" MaxLength="16" Width="100px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorUsername" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtUsername" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblPassword" AssociatedControlID="txtPassword" Text="Password:" />
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="text" Columns="250" MaxLength="250" Width="200px" TextMode="Password" />&nbsp;
                    Repeat: <asp:TextBox ID="txtPassword2" runat="server" CssClass="text" Columns="250" MaxLength="250" Width="200px" TextMode="Password" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtPassword" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status:" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblEditUsers" AssociatedControlID="chkEditUsers" Text="Can edit users:" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkEditUsers" Checked="false" />
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" />
    </div></asp:Content>

