﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ACG_ExitPage.aspx.cs" Inherits="hosting_staticpages_ACG_ExitPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || []; w[l].push({
                        'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                    }); var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-5WLQ44F');</script>
            <!-- End Google Tag Manager -->
    
    <script src="//cdn.zarget.com/94505/142814.js"></script>
    <title>TheAmericanCareerGuide</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/sync-prize-select-how-works.css" rel="stylesheet" />
    <link href="css/ACG_exitpage.css" rel="stylesheet" />
    <link href="css/AdsDisplayMedias.css" rel="stylesheet" />

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    
    <script type="text/javascript" src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/c4r-js/Global.min.js"></script>
    <script type="text/javascript">
        changeFavicon('<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/c4r-images/favicon/acg.ico');
    </script>
       <script>
           $(document).ready(function () {
               //if (!matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
               //    //Desktop Only ads
               //    $('#ad1').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- ACG - Exitpage Top --> <ins class="adsbygoogle ad2" style="display:block;" data-ad-client="ca-pub-0634471641041185" data-ad-slot="6325033149" data-ad-format="auto"></ins><script>        (adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
               //}
               //else {
               //    //Mobile Only ads  
               //    $('#ad1').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- ACG - Exitpage Top --> <ins class="adsbygoogle ad2" style="display:block;" data-ad-client="ca-pub-0634471641041185" data-ad-slot="6325033149" data-ad-format="rectangle"></ins><script>        (adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
               //    $('#ad2').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- ACG - Exitpage Bottom --> <ins class="adsbygoogle ad2" style="display:block;" data-ad-client="ca-pub-0634471641041185" data-ad-slot="8686283230" data-ad-format="auto"></ins><script>        (adsbygoogle = window.adsbygoogle || []).push({});<\/script>');

               //}
               ////Mobile and Desktop Ads

           });
   </script>

</head>
<body runat="server">
            <div class="logo">
                        <a href="/"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/ACG_logo2.png" /></a>
            </div>
                    <div class="section2">
                        
                                     <div class="thankyou">
                                         CONFIRM YOUR EMAIL<br />
                                     </div>
                                      <div class="confirmation">
                                        Check your email for a link to our site where you’ll find the latest job openings in your area.
                                     </div>
                           <div id="ad1">
                               <!-- BEGIN JS TAG - theamericancareerguide.com_728x90_Global_display_Rev_Share < - DO NOT MODIFY -->
                                    <script src="http://select.brealtime.com/ttj?id=12784631&amp;size=728x90" type="text/javascript"></script>
<!-- END TAG -->
                           </div>
                              <div id="ad3">
                                  <!-- BEGIN JS TAG - theamericancareerguide.com_320x50_Global_mobile_web_display_Rev_Share < - DO NOT MODIFY -->
                                <script src="http://select.brealtime.com/ttj?id=12784652&size=320x50" type="text/javascript"></script>
                                <!-- END TAG -->
                           </div>   
                             <div id="ad4">
                                <!-- BEGIN JS TAG - theamericancareerguide.com_300x250_Global_display_Rev_Share < - DO NOT MODIFY -->
                                <script src="http://select.brealtime.com/ttj?id=12784643&size=300x250" type="text/javascript"></script>
                                <!-- END TAG -->
                           </div>    
                        </div>
                    <div class="container-works">
                        <div class="container">
	                    <h3 class="info">How It Works</h3>
                        <div class="row">
    	                    <div class="col-sm-1 col-lg-2 center-block">&nbsp;</div>
      	                    <div class="col-sm-10 col-lg-8 center-block">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <!-- <p>Pick Your Prize</p> -->
                                            <p>Pick Your Interest</p>
                                            <img class="img-responsive center-block" src="/images/omg_icon_form.png" />
                                        </div>
                                        <div class="col-sm-4">
                                            <!-- <p>No Lengthy Waiting Period</p> -->
                                            <p>Confirm Your Email</p>
                                            <img class="img-responsive center-block" src="/images/confirm_icon.png" />
                                        </div>
                                        <div class="col-sm-4">
                                            <!-- <p>Chance to Instantly Win</p> -->
                                            <p>See Your Results</p>
                                            <img class="img-responsive center-block" src="/images/seeresults_icon.png" />
                                        </div>
                                    </div>
                            </div>
      	                    <div class="col-sm-1 col-lg-2 center-block">&nbsp;</div>
                        </div>
                        </div>
                    </div>
            <div class="container-info">
                <div class="container">

                    <div class="row is-flex">
                        <div class="col-xs-12 col-sm-4">
                            <div class="moreinfoinner">
                                <h3>How it works</h3>
                                <p>The American Career Guide is your free resource to help you find, apply to, interview for, and get the job you want.</p>
                                <p><strong>Pick your career area of interest:</strong><br />
                                    Select your area of interest from our three available options. After you have chosen, you will be shown a series of additional questions and offers, which we think might interest you and help with your job search. These questions are designed to help us get to know you better to display relevant and available high paying job opportunities of your interest. </p>
                                <p><strong>Confirm your email:</strong><br />
                                    We sent you an email with a link to click to confirm your email address. This step is important and will help you receive the career information you need. Make sure to use an email address that you regularly check so you won’t miss a great opportunity. </p>
                                <p><strong>See your results:</strong><br />
                                    After providing us with some information, we will display results and you will be given access to listings with employers who are looking to hire in your area and field.</p>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg" style="padding:0px">

                            <div id="ad2">
                                <!-- BEGIN JS TAG - theamericancareerguide.com_300x250_Global_display_Rev_Share < - DO NOT MODIFY -->
                                <script src="http://select.brealtime.com/ttj?id=12784643&size=300x250" type="text/javascript"></script>
                                <!-- END TAG -->
                           </div> 
                         </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="moreinfoinner">
                                <h3>Job Search Resources</h3>
                                <p>Searching for a new job can be hard work. Here are some great resources to help you along the way to employment.<!-- Searching for a new job can be hard work, and we’re here to guide you every step of the way.Beyond our directory there are additional great resources and places to reference for jobs openings.--></p>
                                <p><strong>Networking:</strong><br />
                                    Job search networking is one of the most successful ways to find a new job. Reach out to your contacts - friends, family, neighbors, college alumni - anyone can provide a potential job lead. You can take a direct approach and ask for job leads or be less formal by asking for information and career advice. You might be surprised by your contacts!</p>
                                <p><strong>LinkedIn:</strong><br />
                                    LinkedIn is a powerful professional social network and a great resource for finding a new job. Your LinkedIn profile should read like a resume and the more complete it is, the more chances you have to be found.</p>
                                <p><strong>Social Media:</strong><br />
                                    Using social media is a great way to boost your job search. There has been a huge increase in the number of employers that are using social media sites to recruit and to advertise for employees. Using social media sites as a networking tool can help you find unofficial job postings or find out about job openings through your online connections.</p>

                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="moreinfoinner">
                                <h3>Interview Tips</h3>
                                <p>Below are some great interview tips to help you ace a job interview.</p>


                                <p><strong>Practice and prepare:</strong><br />
                                    Rehearse your responses to the typical job interview questions that most employers ask. It’s also important to remember to tie in relevant job experiences and skills you posses to match the job requirements. The interview is your chance to provide evidence of your previous job successes to promote your candidacy. </p>
                                <p><strong>Research the company and role:</strong><br />
                                    Do your homework on the company and the industry so you are ready to answer questions regarding why you would want to work for them. Your responses should make it obvious that you came prepared.</p>
                                <p><strong>Follow up:</strong><br />
                                    Always follow-up with a thank you note reiterating your interest in the position, within 24 hours of your interview. This is a great time to include any details you may have forgotten to mention during your interview and to highlight again your qualifications.</p>

                                <div class="clear"></div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            


                <div class="clear bottomlinks_content"><div>Copyright &copy; <%=DateTime.Now.Year %> C4R Media Corp.</div><a href="Terms.aspx" target="_blank">Terms and Conditions</a> | <a href="privacy.aspx" target="_blank">Privacy Policy</a><br /></div>

        <script type="text/javascript">
            $(function () {
                dataLayer.push({event: 'acgAnalytics'});
            });
       </script>

</body>
</html>