﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Net;
using System.Text;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Diagnostics;


public partial class hosting_staticpages_ACG_ExitPage : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string email = string.Empty;
            string sessionid = string.Empty;
            string utm_source = string.Empty;
            string utm_medium = string.Empty;

            if (email == "" && Session["ACG_email"] == null)
            {
                email = Request.QueryString["e"];
                Session["ACG_email"] = email;
            }
            else
                email = Session["ACG_email"].ToString();

            if (Request.QueryString["session"] != null)
            {
                sessionid = Request.QueryString["session"];
                Session["sid"] = sessionid;
            }

            if (Request.QueryString["utm_source"] != null)
            {
                utm_source = Request.QueryString["utm_source"];
            }
            if (Request.QueryString["utm_medium"] != null)
            {
                utm_medium = Request.QueryString["utm_medium"];
            }

			var nvc = HttpUtility.ParseQueryString(Request.Url.Query);
			nvc.Remove("e");
			nvc.Remove("first_name");
			nvc.Remove("last_name");

			if (Request.QueryString["e"] != null)
			{
				Response.Redirect("ACG_Exitpage.aspx?" + nvc.ToString());
			}
		}
    }

    //protected void Page_PreRender(object sender, EventArgs e)
    //{
    //    _SaveViewState();
    //}

    //private void _LoadViewState()
    //{
    //    _intPageNo = (int)ViewState["intPageNo"];
    //}

    //private void _SaveViewState()
    //{
    //    ViewState["intPageNo"] = _intPageNo;
    //}
}