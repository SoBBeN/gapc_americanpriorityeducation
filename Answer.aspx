﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPageAnswer.master" CodeFile="Answer.aspx.cs" Inherits="Answer" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="ads_padding">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- HMG - Body --><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-0634471641041185" data-ad-slot="9580178383" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
    </div>
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <br />
    <div id="content">
        The following are links to assistance programs based on your <span style="font-weight:700; color:#f00;">YES</span> responses to the questions you previously answered.<br />
        <br />
        <span class="blue">You can find additional programs by searching the categories in the guide!</span><br />
        <div class="ckedit"><asp:Literal runat="server" ID="litDescription"></asp:Literal></div>
    </div>
</asp:Content>