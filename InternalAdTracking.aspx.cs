﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml;
using System.Collections;
using System.Diagnostics;

public partial class InternalAdTracking : System.Web.UI.Page
{
    [WebMethod]
    public static void track(int adID)
    {
        DB.DbFunctions.InternalAdsTrackingInsert(adID, HttpContext.Current.Request.UrlReferrer.ToString(), HttpContext.Current.Request.UserAgent.ToString(), HttpContext.Current.Request.UserHostAddress.ToString());
    }
}