﻿(function($){
    $.fn.fblikecount = function(options){
        var defaults = {
            baseUrl: 'http://graph.facebook.com/?ids='
        };
        var options = $.extend(defaults, options);
        var count = 0;
        return this.each(function(){
            var $this = $(this);
            $this.hide();
            var objLink = $(this).attr('title');
            if(objLink.indexOf('http') === 0){
                $.getJSON(defaults.baseUrl + objLink + '&callback=?', function (json) {
                    //alert(JSON.stringify(json))
                    if(json[objLink] && json[objLink].shares){
                        $this.append(json[objLink].shares + ' likes');
                        //$this.show();
                    }
                    if(json[objLink] && json[objLink].comments){
                        $this.append(' - ' + json[objLink].comments + ' talking about this');	
                    }
                    $this.show();
                });				
            }
        });
    }
})(jQuery);